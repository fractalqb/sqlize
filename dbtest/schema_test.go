package dbtest

import (
	"database/sql"
	"strings"
	"testing"

	"git.fractalqb.de/fractalqb/sqlize/bsq"
)

func TestVerifyTable(t *testing.T) {

	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()
	_, err = db.Exec(`CREATE TABLE tab (
		id integer PRIMARY KEY
	,	name text NOT NULL
	)`)
	if err != nil {
		t.Fatal(err)
	}

	initOpts := bsq.InitOpts{ColNameMap: strings.ToLower}

	t.Run("happy case", func(t *testing.T) {
		def := struct {
			bsq.Table
			ID, Name bsq.Column
		}{Table: bsq.Table{Name: "tab"}}
		if err := bsq.InitSchema("", &initOpts, &def); err != nil {
			t.Fatal(err)
		}
		ok := VerifyTable(t, db, &def, nil)
		if !ok {
			t.Fatalf("invalid table %s", def.QName())
		}
	})

	t.Run("not in schema", func(t *testing.T) {
		def := struct {
			bsq.Table
			ID bsq.Column
		}{Table: bsq.Table{Name: "tab"}}
		if err := bsq.InitSchema("", &initOpts, &def); err != nil {
			t.Fatal(err)
		}
		ok := VerifyTable(nil, db, &def, nil)
		if ok {
			t.Fatal("missing schema column not detected")
		}
	})

	t.Run("not in DB", func(t *testing.T) {
		def := struct {
			bsq.Table
			ID, Name, Foo bsq.Column
		}{Table: bsq.Table{Name: "tab"}}
		if err := bsq.InitSchema("", &initOpts, &def); err != nil {
			t.Fatal(err)
		}
		ok := VerifyTable(nil, db, &def, nil)
		if ok {
			t.Fatal("missing DB column not detected")
		}
	})
}
