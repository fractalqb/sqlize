package dbtest

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"fmt"
	"os"
	"testing"

	"git.fractalqb.de/fractalqb/sqlize"
)

const (
	ENV_DBDRIVER = "SQLIZE_TESTDB_DRIVER"
	ENV_DBSOURCE = "SQLIZE_TESTDB_SOURCE"
)

// TB has a subset of methods from [testing.TB]. It is implemented by [Errorer].
type TB interface {
	Log(args ...any)
	Error(args ...any)
}

func DriverName(t TB, fallback string) string {
	if d, ok := os.LookupEnv(ENV_DBDRIVER); ok {
		return d
	}
	if fallback != "" {
		return fallback
	}
	msg := fmt.Sprintf("no test DB driver name, set %s in environment", ENV_DBDRIVER)
	if t == nil {
		fmt.Println(msg)
	} else {
		t.Error(msg)
	}
	return ""
}

func SourceName(t TB, fallback string) string {
	if d, ok := os.LookupEnv(ENV_DBSOURCE); ok {
		return d
	}
	msg := fmt.Sprintf("no DB source in %s, using fallback", ENV_DBSOURCE)
	if t == nil {
		fmt.Println(msg)
	} else {
		t.Log(msg)
	}
	if fallback != "" {
		return fallback
	}
	msg = fmt.Sprintf("no test DB source name, set %s in environment", ENV_DBSOURCE)
	if t == nil {
		fmt.Println(msg)
	} else {
		t.Error(msg)
	}
	return ""
}

func OpenArgs(t TB, drvFallback, srcFallback string) (driver, source string) {
	return DriverName(t, drvFallback), SourceName(t, srcFallback)
}

var ExampleTB exampleTB

type exampleTB struct{}

var _ TB = exampleTB{}

func (exampleTB) Error(args ...any) { fmt.Println(args...) }
func (exampleTB) Log(args ...any)   { fmt.Println(args...) }

func Open(t *testing.T, drv driver.Driver, source string, log sqlize.LogFlag) (*sql.DB, error) {
	if source == "" {
		source = SourceName(t, "")
	}
	if log != 0 {
		sqlize.LogDriver(t.Name(), drv, NewLogger(t), log)
		return sql.Open(t.Name(), source)
	}

	// from sql.Open()
	if driverCtx, ok := drv.(driver.DriverContext); ok {
		connector, err := driverCtx.OpenConnector(source)
		if err != nil {
			return nil, err
		}
		return sql.OpenDB(connector), nil
	}
	return sql.OpenDB(dsnConnector{dsn: source, driver: drv}), nil
}

func ShallOpen(t *testing.T, drv driver.Driver, source string, log sqlize.LogFlag) *sql.DB {
	db, err := Open(t, drv, source, log)
	if err != nil {
		t.Fatal(err)
	}
	return db
}

// from sql.Open()
type dsnConnector struct {
	dsn    string
	driver driver.Driver
}

func (t dsnConnector) Connect(_ context.Context) (driver.Conn, error) {
	return t.driver.Open(t.dsn)
}

func (t dsnConnector) Driver() driver.Driver {
	return t.driver
}
