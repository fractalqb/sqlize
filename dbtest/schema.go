package dbtest

import (
	"reflect"
	"testing"

	"git.fractalqb.de/fractalqb/sqlize"
	"git.fractalqb.de/fractalqb/sqlize/bsq"
)

type Dialect interface {
	SQLSelect1Row(table string) string
}

func ColNoConst(t testing.TB, td bsq.TableDef, colNo int) {
	t.Helper()
	if l := len(bsq.Cols(td)); l != colNo {
		t.Fatal(td.TableDef().Name, "defines", l, "columns, const expects", colNo)
	}
}

func VerifyTables(t testing.TB, q sqlize.Querier, d Dialect) (ok bool) {
	ok = true
	bsq.EachTable(func(tbl bsq.TableDef) error {
		ok = VerifyTable(t, q, tbl, d) && ok
		return nil
	})
	return ok
}

func VerifyTable(t testing.TB, q sqlize.Querier, def bsq.TableDef, d Dialect) (ok bool) {
	var sql string
	if d == nil {
		sql = "SELECT * FROM " + def.TableDef().QName()
	} else {
		sql = d.SQLSelect1Row(def.TableDef().QName())
	}
	rows, err := q.Query(sql)
	if err != nil {
		if t != nil {
			t.Error(err)
		}
		return false
	}
	dbCols, err := rows.Columns()
	rows.Close()
	if err != nil {
		if t != nil {
			t.Error(err)
		}
		return false
	}
	var defCols []*bsq.Column
	bsq.EachColumn(def, func(c *bsq.Column, _ *reflect.StructField) error {
		defCols = append(defCols, c)
		return nil
	})
	ok = true
	for _, dbCol := range dbCols {
		found := false
		for _, defCol := range defCols {
			if defCol.Name == dbCol {
				found = true
				break
			}
		}
		if !found {
			if t != nil {
				t.Errorf("DB column '%s' of table '%s' not found in schema",
					dbCol,
					def.TableDef().QName(),
				)
			}
			ok = false
		}
	}
	for _, defCol := range defCols {
		found := false
		for _, dbCol := range dbCols {
			if dbCol == defCol.Name {
				found = true
				break
			}
		}
		if !found {
			if t != nil {
				t.Errorf("schema column '%s' of %s not found in DB",
					defCol.Name,
					def.TableDef().QName(),
				)
			}
			ok = false
		}
	}
	return ok
}
