package dbtest

import (
	"sync/atomic"

	"git.fractalqb.de/fractalqb/sqlize"
)

// NewLogger creates a new logger to be used with sqlize.LogDriver. For an
// example read the test TestNewLogger.
func NewLogger(t interface{ Logf(string, ...any) }) sqlize.Logger {
	return &logger{t: t}
}

type logger struct {
	t interface{ Logf(string, ...any) }
	n uint64
}

func (l *logger) Call(name string, args ...any) sqlize.CallRef {
	r := atomic.AddUint64(&l.n, 1)
	if args == nil {
		l.t.Logf("%d %s ↷", r, name)
	} else {
		l.t.Logf("%d %s ↷ %+v", r, name, args)
	}
	return sqlize.CallRef{Name: name, ID: r}
}

func (l *logger) Result(ref sqlize.CallRef, err error, args ...any) {
	if err == nil {
		if args == nil {
			l.t.Logf("%d %s ✓", ref.ID, ref.Name)
		} else {
			l.t.Logf("%d %s ✓ %+v", ref.ID, ref.Name, args)
		}
	} else {
		if args == nil {
			l.t.Logf("%d %s ↯ %s", ref.ID, ref.Name, err)
		} else {
			l.t.Logf("%d %s ↯ %s (%+v)", ref.ID, ref.Name, err, args)
		}
	}
}
