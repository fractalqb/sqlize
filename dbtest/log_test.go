package dbtest

import (
	"database/sql"
	"testing"

	"github.com/mattn/go-sqlite3"

	"git.fractalqb.de/fractalqb/sqlize"
)

func TestNewLogger(t *testing.T) {
	const LogDriverName = "log-sqlite3"
	sqlize.LogDriver(
		LogDriverName, &sqlite3.SQLiteDriver{},
		NewLogger(t), sqlize.LogAll,
	)
	db, err := sql.Open("log-sqlite3", ":memory:")
	if err != nil {
		t.Fatal(err)
	}
	db.QueryRow(`select * from yat`)
	defer db.Close()
	// TODO howto verify that t logs the expected events?
}
