package dbtest

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"testing"

	"git.fractalqb.de/fractalqb/sqlize"
	"git.fractalqb.de/fractalqb/sqlize/entity"
)

type CRUDTest[E entity.Entity[I], I entity.ID] struct {
	Repo         entity.Repo[E, I]
	Create, Read *E
	Equal        func(e, f *E) error
	changes      []change
}

type FieldDiff []error

func (d FieldDiff) Error() error {
	if len(d) == 0 {
		return nil
	}
	return errors.Join(d...)
}

func FieldEq[F comparable](e FieldDiff, hint string, l, r F) FieldDiff {
	if l == r {
		return e
	}
	return append(e, fmt.Errorf("%s: %+v != %+v", hint, l, r))
}

func FieldEqual[F interface{ Equal(F) bool }](e FieldDiff, hint string, l, r F) FieldDiff {
	if l.Equal(r) {
		return e
	}
	return append(e, fmt.Errorf("%s: !Equal(%+v, %+v)", hint, l, r))
}

func (crud CRUDTest[E, I]) Update(name string, mod func(*E)) CRUDTest[E, I] {
	crud.changes = append(crud.changes, change{name, mod})
	return crud
}

func (crud CRUDTest[E, I]) Run(t *testing.T, db sqlize.Querier) {
	eTypeName := reflect.Indirect(reflect.ValueOf(crud.Create)).Type().Name()

	t.Run(fmt.Sprintf("CRUD create %s", eTypeName), func(t *testing.T) {
		var err error
		crud.Create, err = crud.Repo.Create(db, crud.Create)
		if err != nil {
			t.Fatal(err)
		}
		if id := (*crud.Create).EntityID(); id == sqlize.NoID {
			t.Fatalf("invalid entity ID %d", id)
		}
	})

	t.Run(fmt.Sprintf("CRUD read %s", eTypeName), func(t *testing.T) {
		var err error
		crud.Read, err = crud.Repo.ReadContext(context.Background(),
			db,
			(*crud.Create).EntityID(),
			crud.Read,
		)
		if err != nil {
			t.Fatal(err)
		}
		if rid, cid := (*crud.Read).EntityID(), (*crud.Create).EntityID(); rid != cid {
			t.Errorf("wrong id %d, expect %d", rid, cid)
		}
		if err := crud.equal(crud.Read, crud.Create); err != nil {
			t.Log(crud.Create)
			t.Log(crud.Read)
			t.Error("create/read:", err)
		}
	})

	for _, mod := range crud.changes {
		modify := mod.mod.(func(*E))
		t.Run(fmt.Sprintf("CRUD update %s change %s", eTypeName, mod.name), func(t *testing.T) {
			modify(crud.Create)
			err := crud.Repo.Update(db, crud.Create)
			if err != nil {
				t.Fatal(err)
			}
			crud.Read, err = crud.Repo.ReadContext(context.Background(),
				db,
				(*crud.Create).EntityID(),
				crud.Read,
			)
			if err != nil {
				t.Fatal(err)
			}
			if err := crud.equal(crud.Read, crud.Create); err != nil {
				t.Log(crud.Create)
				t.Log(crud.Read)
				t.Error("create/read:", err)
			}
		})
	}

	t.Run(fmt.Sprintf("CRUD delete %s", eTypeName), func(t *testing.T) {
		err := crud.Repo.Delete(db, (*crud.Create).EntityID())
		if err != nil {
			t.Fatal(err)
		}
		crud.Read, err = crud.Repo.ReadContext(context.Background(),
			db,
			(*crud.Create).EntityID(),
			crud.Read,
		)
		switch {
		case err == nil:
			t.Errorf("%s with ID %d not deleted", eTypeName, (*crud.Create).EntityID())
		case err != sql.ErrNoRows:
			t.Error(err)
		}
	})
}

func (crud *CRUDTest[E, I]) equal(e, f *E) error {
	if crud.Equal == nil {
		if !reflect.DeepEqual(e, f) {
			return errors.New("not DeepEqual")
		}
		return nil
	}
	return crud.Equal(e, f)
}

type change struct {
	name string
	mod  any
}
