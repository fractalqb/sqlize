package sqlize

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/mattn/go-sqlite3"
)

func ExampleLogDriver() {
	const loggingDriverName = "log-sqlite3"
	LogDriver(
		loggingDriverName, &sqlite3.SQLiteDriver{},
		NewStdLogger(log.New(os.Stdout, "DB: #", 0)),
		LogAll,
	)
	db, err := sql.Open(loggingDriverName, ":memory:")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()
	db.QueryRow("SELECT * FROM sqlize")
	// Output:
	// DB: #1     Connect
	// DB: #1 OK  Connect
	// DB: #2     Query [SELECT * FROM sqlize []]
	// DB: #2 ERR Query no such table: sqlize
}
