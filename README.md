# sqlize

[![Go Reference](https://pkg.go.dev/badge/git.fractalqb.de/fractalqb/sqlize.svg)](https://pkg.go.dev/git.fractalqb.de/fractalqb/sqlize)

Experimental tools to help with the use of Go sql. It strives not to be a
framework but a toolbox that makes it easy to pick only the tools you want.

This module is somewhat chaotic. It is primarily published as a
dependency off another FOSS app project. However the `sqlize` has
matured. And, by the way, it should combine quite well with sqlx.

Key features are:

* Easily handle transactions with a "begin at most once" semantic. See
  `sqlize.Xaction` and `stmt.Xaction`.

* Query building that hepls to keep queries in line with the DB
  model. See package `bsq`.

* Log DB calls to Go's standard logger or tests. Can easily extended for other
  loggers.

* Entities and repositories

* Supports using the above concepts with prepared statements.
