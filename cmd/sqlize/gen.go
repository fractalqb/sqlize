package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"slices"

	"git.fractalqb.de/fractalqb/nmconv"
	"git.fractalqb.de/fractalqb/sqlize/bsq"
)

type cmdGen struct {
	colPerLine int
	mkAlias    bool
	colNames   bool
	colNo      bool
}

var gen = cmdGen{colPerLine: 8}

func (gen *cmdGen) run(args []string) {
	gen.flags(args)
	tabs := readTables()
	if gen.mkAlias {
		makeAliases(tabs)
	}
	slices.SortFunc(tabs, cmprByScnNm)
	for i, t := range tabs {
		if i > 0 {
			fmt.Println()
		}
		gen.write(os.Stdout, t)
	}
}

func (gen *cmdGen) flags(args []string) []string {
	flags := flag.NewFlagSet("gen", flag.ExitOnError)
	flags.IntVar(&gen.colPerLine, "cpl", gen.colPerLine, "Columns per source line")
	flags.BoolVar(&gen.mkAlias, "alias", gen.mkAlias, "Generate default aliases")
	flags.BoolVar(&gen.colNames, "colnames", gen.colNames, "Force column names")
	flags.BoolVar(&gen.colNo, "colno", gen.colNo, "Write column number constant")
	flags.Parse(args[1:])
	return flags.Args()
}

func (gen *cmdGen) write(w io.Writer, tab *table) {
	tabVarName := nmToGo.Convert(tab.table)
	if gen.colNo {
		fmt.Fprintf(w, "const T%sColNo = %d\n\n", tabVarName, len(tab.columns))
	}
	fmt.Fprintf(w, "var T%s = struct {\n", tabVarName)
	fmt.Fprintln(w, "\tbsq.Table")
	for i, col := range tab.columns {
		if i == 0 {
			fmt.Fprint(w, "\t")
		} else if gen.colPerLine > 0 && i%gen.colPerLine == 0 {
			fmt.Fprint(w, " bsq.Column\n\t")
		} else {
			fmt.Fprint(w, ", ")
		}
		fmt.Fprint(w, colToGo(col.Name()))
	}
	fmt.Fprintln(w, " bsq.Column\n}{")
	fmt.Fprintf(w, "\tTable: bsq.Table{Name: \"%s\"", tab.table)
	if tab.alias != "" {
		fmt.Fprintf(w, ", DefaultAlias: \"%s\"", tab.alias)
	}
	fmt.Fprintln(w, "},")
	bsqNmMap := bsq.DefaultNameMap()
	for _, col := range tab.columns {
		goCol := colToGo(col.Name())
		sqlCol := bsqNmMap(goCol)
		if !gen.colNames {
			if sqlCol == col.Name() {
				continue
			}
		}
		fmt.Fprintf(w, "\t%s: bsq.Column{Name: \"%s\"},\n", goCol, col.Name())
	}
	fmt.Fprintln(w, "}")
}

var nmToGo = nmconv.Conversion{
	Norm:   nmconv.Unsep("_"),
	Denorm: nmconv.Camel1Up,
}

func colToGo(n string) string {
	switch n {
	case "id":
		return "ID"
	}
	return nmToGo.Convert(n)
}

func makeAliases(tabs []*table) {
	slices.SortFunc(tabs, func(l, r *table) int { return len(l.table) - len(r.table) })
	as := make(map[string]int)
	for i, tab := range tabs {
		runes := []rune(tab.table)
		runes = slices.DeleteFunc(runes, func(c rune) bool { return c == '_' })
		alias := string(runes[0])
		runes = runes[1:]
	CHECK_ALIAS:
		for _, ok := as[alias]; ok && len(runes) > 0; _, ok = as[alias] {
			for _, r := range runes {
				tmp := alias + string(r)
				if _, ok := as[tmp]; !ok {
					alias = tmp
					continue CHECK_ALIAS
				}
			}
			alias = alias + string(runes[0])
			runes = runes[1:]
		}
		if _, ok := as[alias]; !ok {
			as[alias] = i
		}
	}
	for a, i := range as {
		tab := tabs[i]
		tab.alias = a
	}
}
