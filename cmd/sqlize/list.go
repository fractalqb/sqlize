package main

import (
	"fmt"
	"os"
	"slices"
	"text/tabwriter"
)

func listTables() {
	ts := readTables()
	slices.SortFunc(ts, cmprByScnNm)
	tw := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
	defer tw.Flush()
	for _, t := range ts {
		fmt.Fprintf(tw, "%s\t%s", t.schema, t.table)
		if t.alias != "" {
			fmt.Fprintf(tw, "\t(%s)", t.alias)
		}
		fmt.Fprintln(tw)
	}
}
