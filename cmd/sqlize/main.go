package main

import (
	"database/sql"
	"encoding/hex"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/yacfg"
	"git.fractalqb.de/fractalqb/yacfg/yasec"
	"github.com/jimsmart/schema"
	"golang.org/x/term"

	_ "github.com/jackc/pgx/v5/stdlib"
	_ "github.com/mattn/go-sqlite3"
)

const yasecSalt = "d2758862dca686c011670d8900cc6b84"

var (
	cfg = struct {
		Driver  string
		DB      yasec.Secret
		Schemas string
		Tables  string
	}{
		Schemas: ".+",
		Tables:  ".+",
	}
	dbSrc string
)

func flags() {
	flag.StringVar(&cfg.Driver, "d", cfg.Driver,
		fmt.Sprintf("Select SQL driver from: %s", strings.Join(sql.Drivers(), ", ")),
	)
	flag.StringVar(&dbSrc, "db", dbSrc, "DB connect string")
	flag.StringVar(&cfg.Schemas, "s", cfg.Schemas, "Regexp to filter schema names")
	flag.StringVar(&cfg.Tables, "t", cfg.Tables, "Regexp to filter table names")
	flag.Parse()
}

func usage() {
	w := flag.CommandLine.Output()
	fmt.Fprintln(w, `Usage: sqlize [flags] <command>
COMMANDS
  list: List tables from database
  gen : Generate sqlize/bsq tables
FLAGS`)
	flag.PrintDefaults()
}

func main() {
	flag.Usage = usage
	yacfg.FromEnvThenFiles{
		EnvPrefix:     "SQLIZE_",
		FilesFlagName: "cfg",
		Flags:         flag.CommandLine,
		Log:           func(m string) { log.Print(m) },
	}.MustConfigure(&cfg)
	flags()

	defer func() {
		if p := recover(); p != nil {
			log.Println(p)
		}
	}()

	if flag.NArg() == 0 {
		listTables()
		return
	}

	switch flag.Arg(0) {
	case "list":
		listTables()
	case "gen":
		gen.run(flag.Args())
	}
}

type table struct {
	schema  string
	table   string
	alias   string
	columns []*sql.ColumnType
}

func cmprByScnNm(l, r *table) int {
	cmpr := strings.Compare(l.schema, r.schema)
	if cmpr == 0 {
		cmpr = strings.Compare(l.table, r.table)
	}
	return cmpr
}

func readTables() (tables []*table) {
	scmFilter := must.Ret(regexp.Compile(cfg.Schemas))
	tblFilter := must.Ret(regexp.Compile(cfg.Tables))
	var db *sql.DB
	if dbSrc != "" {
		db = must.Ret(sql.Open(cfg.Driver, dbSrc))
	} else {
		salt := must.Ret(hex.DecodeString(yasecSalt))
		if term.IsTerminal(int(os.Stdin.Fd())) {
			must.Do(yasec.DefaultConfig.SetFromPrompt("Config password:", salt))
		} else {
			yasec.DefaultConfig.SetFromUnixSocket("sqlize.yasec", salt)
		}
		dbBuf := must.Ret(cfg.DB.Open())
		var err error
		db, err = sql.Open(
			cfg.Driver,
			strings.Clone(dbBuf.String()), // NOTE: escape memguard
		)
		dbBuf.Destroy()
		must.Do(err)
	}
	defer db.Close()

	tbls := must.Ret(schema.Tables(db))
	for scmtab, cols := range tbls {
		if !scmFilter.MatchString(scmtab[0]) {
			continue
		}
		if !tblFilter.MatchString(scmtab[1]) {
			continue
		}
		tables = append(tables, &table{
			schema:  scmtab[0],
			table:   scmtab[1],
			columns: cols,
		})
	}

	return tables
}
