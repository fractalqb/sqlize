package null

import "database/sql/driver"

var (
	Ignore ignType
	Value  nullType
)

type ignType struct{}

func (ignType) Scan(value interface{}) error { return nil }

type nullType struct{}

func (nullType) Value() (driver.Value, error) { return nil, nil }
