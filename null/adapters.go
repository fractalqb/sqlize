// Package null implements typed adapters for Go values to nullable DB columns.
package null

import (
	"database/sql/driver"
	"fmt"
	"math"
	"time"

	"golang.org/x/exp/constraints"
)

func IfEq[T comparable](v, null T) any {
	if v == null {
		return nil
	}
	return v
}

func IfLe[T constraints.Ordered](v, cmpr T) any {
	if v <= cmpr {
		return nil
	}
	return v
}

func IfLt[T constraints.Ordered](v, cmpr T) any {
	if v < cmpr {
		return nil
	}
	return v
}

func If[T any](v T, pred func(T) bool) any {
	if pred(v) {
		return nil
	}
	return v
}

type Bool struct {
	P    *bool
	Null bool
}

func (b Bool) Scan(value interface{}) error {
	if value == nil {
		*b.P = b.Null
		return nil
	}
	switch v := value.(type) {
	case bool:
		*b.P = v
	default:
		return fmt.Errorf("cannot scan null Int from %T", value)
	}
	return nil
}

func (b Bool) Value() (driver.Value, error) {
	if *b.P == b.Null {
		return nil, nil
	}
	return bool(*b.P), nil
}

type Int struct {
	P    *int
	Null int
}

func (i Int) Scan(value interface{}) error {
	if value == nil {
		*i.P = i.Null
		return nil
	}
	switch v := value.(type) {
	case int64:
		if v < math.MinInt || v > math.MaxInt {
			return fmt.Errorf("%d out of int range", v)
		}
		*i.P = int(v)
	case int:
		*i.P = v
	default:
		return fmt.Errorf("cannot scan null Int from %T", value)
	}
	return nil
}

func (i Int) Value() (driver.Value, error) {
	if *i.P == i.Null {
		return nil, nil
	}
	return int64(*i.P), nil
}

type Int16 struct {
	P    *int16
	Null int16
}

func (i Int16) Scan(value interface{}) error {
	if value == nil {
		*i.P = i.Null
		return nil
	}
	switch v := value.(type) {
	case int64:
		if v < math.MinInt16 || v > math.MaxInt16 {
			return fmt.Errorf("%d out of int16 range", v)
		}
		*i.P = int16(v)
	case int16:
		*i.P = v
	default:
		return fmt.Errorf("cannot scan null Int16 from %T", value)
	}
	return nil
}

func (i Int16) Value() (driver.Value, error) {
	if *i.P == i.Null {
		return nil, nil
	}
	return int64(*i.P), nil
}

type Int32 struct {
	P    *int32
	Null int32
}

func (i Int32) Scan(value interface{}) error {
	if value == nil {
		*i.P = i.Null
		return nil
	}
	switch v := value.(type) {
	case int64:
		if v < math.MinInt32 || v > math.MaxInt32 {
			return fmt.Errorf("%d out of int32 range", v)
		}
		*i.P = int32(v)
	case int32:
		*i.P = v
	default:
		return fmt.Errorf("cannot scan null Int32 from %T", value)
	}
	return nil
}

func (i Int32) Value() (driver.Value, error) {
	if *i.P == i.Null {
		return nil, nil
	}
	return int64(*i.P), nil
}

type Int64 struct {
	P    *int64
	Null int64
}

func (i Int64) Scan(value interface{}) error {
	if value == nil {
		*i.P = i.Null
		return nil
	}
	switch v := value.(type) {
	case int64:
		*i.P = v
	default:
		return fmt.Errorf("cannot scan null Int64 from %T", value)
	}
	return nil
}

func (i Int64) Value() (driver.Value, error) {
	if *i.P == i.Null {
		return nil, nil
	}
	return *i.P, nil
}

type UInt16 struct {
	P    *uint16
	Null uint16
}

func (i UInt16) Scan(value interface{}) error {
	if value == nil {
		*i.P = i.Null
		return nil
	}
	switch v := value.(type) {
	case int64:
		if v < 0 || v > math.MaxUint16 {
			return fmt.Errorf("%d out of uint16 range", v)
		}
		*i.P = uint16(v)
	default:
		return fmt.Errorf("cannot scan null UInt16 from %T", value)
	}
	return nil
}

func (i UInt16) Value() (driver.Value, error) {
	if *i.P == i.Null {
		return nil, nil
	}
	return int64(*i.P), nil
}

type UInt32 struct {
	P    *uint32
	Null uint32
}

func (i UInt32) Scan(value interface{}) error {
	if value == nil {
		*i.P = i.Null
		return nil
	}
	switch v := value.(type) {
	case int64:
		if v < 0 || v > math.MaxUint32 {
			return fmt.Errorf("%d out of uint32 range", v)
		}
		*i.P = uint32(v)
	case uint32:
		*i.P = v
	default:
		return fmt.Errorf("cannot scan null UInt32 from %T", value)
	}
	return nil
}

func (i UInt32) Value() (driver.Value, error) {
	if *i.P == i.Null {
		return nil, nil
	}
	return *i.P, nil
}

type UInt64 struct {
	P    *uint64
	Null uint64
}

func (i UInt64) Scan(value interface{}) error {
	if value == nil {
		*i.P = i.Null
		return nil
	}
	switch v := value.(type) {
	case int64:
		if v < 0 {
			return fmt.Errorf("cannot scan null UInt64 from %d", v)
		}
		*i.P = uint64(v)
	case uint64:
		*i.P = v
	default:
		return fmt.Errorf("cannot scan null UInt64 from %T", value)
	}
	return nil
}

func (i UInt64) Value() (driver.Value, error) {
	if *i.P == i.Null {
		return nil, nil
	}
	return int64(*i.P), nil
}

type String struct {
	P    *string
	Null string
}

func (s String) Scan(value interface{}) error {
	if value == nil {
		*s.P = s.Null
		return nil
	}
	switch v := value.(type) {
	case string:
		*s.P = v
	default:
		return fmt.Errorf("cannot scan null string from %T", value)
	}
	return nil
}

func (s String) Value() (driver.Value, error) {
	if *s.P == s.Null {
		return nil, nil
	}
	return *s.P, nil
}

type Float32 struct {
	P    *float32
	Null float32
	NaN  interface{}
}

func (f Float32) Map(p *float32) Float32 { return Float32{p, f.Null, f.NaN} }

func (f Float32) Scan(value interface{}) error {
	if value == nil {
		*f.P = f.Null
		return nil
	}
	switch v := value.(type) {
	case float64:
		*f.P = float32(v)
	default:
		return fmt.Errorf("cannot scan null float32 from %T", value)
	}
	return nil
}

func (f Float32) Value() (driver.Value, error) {
	if math.IsNaN(float64(*f.P)) {
		return f.NaN, nil
	}
	return float64(*f.P), nil
}

type Float64 struct {
	P    *float64
	Null float64
	NaN  interface{}
}

func (f Float64) Map(p *float64) Float64 { return Float64{p, f.Null, f.NaN} }

func (f Float64) Scan(value interface{}) error {
	if value == nil {
		*f.P = f.Null
		return nil
	}
	switch v := value.(type) {
	case float64:
		*f.P = v
	default:
		return fmt.Errorf("cannot scan null float64 from %T", value)
	}
	return nil
}

func (f Float64) Value() (driver.Value, error) {
	if math.IsNaN(*f.P) {
		return f.NaN, nil
	}
	return *f.P, nil
}

type Time struct {
	P *time.Time
}

func (s Time) Scan(value interface{}) error {
	if value == nil {
		*s.P = time.Time{}
		return nil
	}
	switch v := value.(type) {
	case time.Time:
		*s.P = v
	default:
		return fmt.Errorf("cannot scan null Time from %T", value)
	}
	return nil
}

func (s Time) Value() (driver.Value, error) {
	if s.P == nil || s.P.IsZero() {
		return nil, nil
	}
	return *s.P, nil
}

type Bytes struct {
	P       *[]byte
	ZeroLen bool
}

func (s Bytes) Scan(value interface{}) error {
	if value == nil {
		*s.P = nil
		return nil
	}
	switch v := value.(type) {
	case []byte:
		if s.ZeroLen && len(v) == 0 {
			*s.P = nil
		} else {
			*s.P = v
		}
	default:
		return fmt.Errorf("cannot scan null Bytes from %T", value)
	}
	return nil
}

func (s Bytes) Value() (driver.Value, error) {
	if s.P == nil {
		return nil, nil
	} else if s.ZeroLen && len(*s.P) == 0 {
		return nil, nil
	}
	return *s.P, nil
}
