/*
Package sqlize helps with on-demand transactions, SQL logging and running
scripts with Go's standard database/sql package.

# Moular DB Code with Transactions

In Go the object to perform DB operations is explicitly passed around – for good
reasons. These objects are either of type *sql.DB and *sql.Tx. If one wants to
perfom a single atomic DB operation its fast and simple to use sql.DB.Exec et
al. If one needs to combine more than one operation into a tarnsaction, then
sql.Tx.Exed et al must be used. Now consider a DB function

	func simpleDBOp(db *sql.DB) error {
	    … a single DB operation …
	}

Nice and smooth! Now we want to use it in a complex DB transaction:

	func complexDBOp(tx *sql.Tx) error { // complex ⇒ need a Tx
	    …
	    simpleDBOp(tx) // compile error
	    …
	}

One would have to rewrite simpleDBOp(tx *sql.Tx)… which imposes the costs of
explicit transactions to all users of simpleDBOp. That's why sqlize uses the
Querier interface that has the common methods of sql.DB and sql.Tx. This makes
the sqlize API reusable and allows to implement application code in a more
modular way:

	// Now, complexDBOp(tx *sql.Tx) will compile
	func simpleDBOp(db sqlize.Querier) error {
	    … a single DB operation …
	}
*/
package sqlize

//go:generate gosec ./...
