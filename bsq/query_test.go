package bsq

import (
	"fmt"
	"testing"

	. "git.fractalqb.de/fractalqb/sqlize/bsq/keywords"
)

func ExampleQuery() {
	testTable := struct {
		Table
		ID, Name Column
	}{Table: Table{Name: "test", DefaultAlias: "t"}}
	InitSchema("", nil, &testTable)

	testAlias := testTable.As("t2")

	q, err := NewQuery(testDialect,
		SELECT, Out(&testTable.ID, testTable.Name.In(testAlias)),
		FROM, Ls(", ").Of(&testTable, testAlias),
		WHERE, P("name"), IS_NOT_NULL,
		AND, PGe(&testTable.ID), AND, PLt(&testTable.ID),
	)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(q.String())
	fmt.Println("       ^ o1  ^o2                                ^ p1              ^ p2        ^ p3")
	fmt.Print("Param Columns")
	for i, c := range q.pCols {
		if c == nil {
			fmt.Printf(" %d:-", i+1)
		} else {
			fmt.Printf(" %d:%s", i+1, c.QName())
		}
	}
	fmt.Print("\nOutput")
	for i, c := range q.oCols {
		if c == nil {
			fmt.Printf(" %d:-", i+1)
		} else {
			fmt.Printf(" %d:%s", i+1, c.QName())
		}
	}
	// Output:
	// SELECT t.ID, t2.Name FROM test t, test t2 WHERE ? IS NOT NULL AND t.ID>=? AND t.ID<?
	//        ^ o1  ^o2                                ^ p1              ^ p2        ^ p3
	// Param Columns 1:- 2:test.ID 3:test.ID
	// Output 1:test.ID 2:test.Name
}

func Test_eqJoin(t *testing.T) {
	t1 := testTable.As("t1")
	t2 := testTable.As("t2")
	t.Run("join", func(t *testing.T) {
		q, err := NewQuery(testDialect, JoinEq(testTable.ID.In(t1), testTable.Name.In(t2)))
		if err != nil {
			t.Fatal(err)
		}
		if q.String() != " JOIN thing t2 ON (t1.ID=t2.Name)" {
			t.Error(q.String())
		}
	})
	t.Run("left join", func(t *testing.T) {
		q, err := NewQuery(testDialect, LeftJoinEq(testTable.ID.In(t1), testTable.Name.In(t2)))
		if err != nil {
			t.Fatal(err)
		}
		if q.String() != " LEFT JOIN thing t2 ON (t1.ID=t2.Name)" {
			t.Error(q.String())
		}
	})
	t.Run("right join", func(t *testing.T) {
		q, err := NewQuery(testDialect, RightJoinEq(testTable.ID.In(t1), testTable.Name.In(t2)))
		if err != nil {
			t.Fatal(err)
		}
		if q.String() != " RIGHT JOIN thing t2 ON (t1.ID=t2.Name)" {
			t.Error(q.String())
		}
	})
	t.Run("full join", func(t *testing.T) {
		q, err := NewQuery(testDialect, FullJoinEq(testTable.ID.In(t1), testTable.Name.In(t2)))
		if err != nil {
			t.Fatal(err)
		}
		if q.String() != " FULL JOIN thing t2 ON (t1.ID=t2.Name)" {
			t.Error(q.String())
		}
	})
}

func ExampleP() {
	example := func(bv func() BindVars, p, q any) {
		qry, _ := NewQuery(DefaultSQL(bv, nil), p, AND, q)
		fmt.Println(len(qry.ParamCols()), "parameters:", qry.String())
	}

	example(PositionalVars("?"), P("p1"), P("p1"))

	example(IndexedVars("$%d"), P("p1"), P("p1"))
	example(IndexedVars("$%d"), P("p1"), P("p2"))

	example(NamedVars(":%s"), P("p1"), P("p1"))
	example(NamedVars(":%s"), P("p1"), P("p2"))

	// Output:
	// 2 parameters: ? AND ?
	// 1 parameters: $1 AND $1
	// 2 parameters: $1 AND $2
	// 1 parameters: :p1 AND :p1
	// 2 parameters: :p1 AND :p2
}

func ExamplePEq() {
	q, _ := NewQuery(testDialect, PEq(&testTable.Name))
	fmt.Println(q.String())
	// Output:
	// Name=?
}

func ExamplePNe() {
	q, _ := NewQuery(testDialect, PNe(&testTable.Name))
	fmt.Println(q.String())
	// Output:
	// Name<>?
}

func ExamplePLt() {
	q, _ := NewQuery(testDialect, PLt(&testTable.Name))
	fmt.Println(q.String())
	// Output:
	// Name<?
}

func ExamplePLe() {
	q, _ := NewQuery(testDialect, PLe(&testTable.Name))
	fmt.Println(q.String())
	// Output:
	// Name<=?
}

func ExamplePGt() {
	q, _ := NewQuery(testDialect, PGt(&testTable.Name))
	fmt.Println(q.String())
	// Output:
	// Name>?
}

func ExamplePGe() {
	q, _ := NewQuery(testDialect, PGe(&testTable.Name))
	fmt.Println(q.String())
	// Output:
	// Name>=?
}
