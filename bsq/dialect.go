package bsq

import "fmt"

type Dialect interface {
	NewContext() DialectCtx
	Quote(name string) string
	Parameter(ctx DialectCtx, key string, col *Column) (string, error)
}

type DialectCtx interface {
	WriteContext() *WriteCtx
	ParamColumns() []*Column
	OutColumns() []*Column
	Close()
}

type PositionBindVars struct {
	Symbol string
	params []*Column
}

func (bv *PositionBindVars) Param(_ string, c *Column) (string, error) {
	bv.params = append(bv.params, c)
	return bv.Symbol, nil
}

func (bv *PositionBindVars) ParamColumns() []*Column { return bv.params }

type IndexBindVars struct {
	fmt    string
	params []*Column
	keyIdx map[string]int
}

func NewIndexBindVars(format string) *IndexBindVars {
	return &IndexBindVars{
		fmt:    format,
		keyIdx: make(map[string]int),
	}
}

func (bv *IndexBindVars) Param(key string, c *Column) (string, error) {
	if idx, ok := bv.keyIdx[key]; ok {
		if c == nil {
			if oc := bv.params[idx]; oc != nil && oc != c {
				on, cn := "-", "-"
				if oc != nil {
					on = oc.AName()
				}
				if c != nil {
					cn = c.AName()
				}
				return "", fmt.Errorf("binding param to different columns %s and %s", on, cn)
			}
			bv.params[idx] = c
		}
		return fmt.Sprintf(bv.fmt, idx+1), nil
	}
	bv.keyIdx[key] = len(bv.params)
	bv.params = append(bv.params, c)
	return fmt.Sprintf(bv.fmt, len(bv.params)), nil
}

func (bv *IndexBindVars) ParamColumns() []*Column { return bv.params }

type NameBindVars struct {
	fmt    string
	params []*Column
	keyIdx map[string]int
}

func NewNamedBindVars(format string) *NameBindVars {
	return &NameBindVars{
		fmt:    format,
		keyIdx: make(map[string]int),
	}
}

func (bv *NameBindVars) Param(key string, c *Column) (string, error) {
	if idx, ok := bv.keyIdx[key]; ok {
		if c != nil {
			if oc := bv.params[idx]; oc != nil && oc != c {
				on, cn := "-", "-"
				if oc != nil {
					on = oc.AName()
				}
				if c != nil {
					cn = c.AName()
				}
				return "", fmt.Errorf("binding param to different columns %s and %s", on, cn)
			}
			bv.params[idx] = c
		}
	} else {
		bv.keyIdx[key] = len(bv.params)
		bv.params = append(bv.params, c)
	}
	return fmt.Sprintf(bv.fmt, key), nil
}

func (bv *NameBindVars) ParamColumns() []*Column { return bv.params }
