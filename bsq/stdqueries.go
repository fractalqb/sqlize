package bsq

import (
	"errors"

	. "git.fractalqb.de/fractalqb/sqlize/bsq/keywords"
)

type Insert struct {
	Table   TableDef
	Columns Columns
}

func (ins Insert) Defn() []any {
	if ins.Columns == nil {
		ins.Columns = Cols(ins.Table)
	}
	if len(ins.Columns) == 0 {
		return nil
	}
	if ins.Table == nil {
		ins.Table = ins.Columns[0].Table()
	}
	params := make([]any, len(ins.Columns))
	for i, c := range ins.Columns {
		params[i] = P(c)
	}
	return []any{
		INSERT_INTO, ins.Table, ' ',
		List{Elems: ins.Columns.Defn(nil), Pre: "(", Sep: ", ", Post: ")"},
		VALUES,
		List{Elems: params, Pre: "(", Sep: ", ", Post: ")"},
	}
}

type Select tabColsKey

func (sel Select) Defn() []any {
	if sel.Columns == nil && sel.Key == nil {
		return []any{SELECT, '*', FROM, sel.Table}
	}
	if err := (*tabColsKey)(&sel).complete(); err != nil {
		return nil
	}
	var out any
	if len(sel.Columns) == 0 {
		out = '*'
	} else {
		out = Out(sel.Columns.Defn(nil)...)
	}
	if sel.Table == nil {
		sel.Table = sel.Columns[0].Table()
	}
	res := []any{SELECT, out, FROM, sel.Table}
	for i, k := range sel.Key {
		if i > 0 {
			res = append(res, AND, PEq(k))
		} else {
			res = append(res, WHERE, PEq(k))
		}
	}
	return res
}

type Update tabColsKey

func (upd Update) Defn() []any {
	if err := (*tabColsKey)(&upd).complete(); err != nil {
		return nil
	}
	res := []any{UPDATE, upd.Table, SET}
	for i, c := range upd.Columns {
		if i > 0 {
			res = append(res, ", ")
		}
		res = append(res, PEq(c))
	}
	if len(upd.Key) > 0 {
		res = append(res, WHERE, PEq(upd.Key[0]))
		for _, c := range upd.Key[1:] {
			res = append(res, AND, PEq(c))
		}
	}
	return res
}

type Delete struct {
	Key Columns
}

func (del Delete) Defn() []any {
	if len(del.Key) == 0 {
		return nil
	}
	res := []any{DELETE_FROM, del.Key[0].Table()}
	if len(del.Key) > 0 {
		res = append(res, WHERE, PEq(del.Key[0]))
		for _, c := range del.Key[1:] {
			res = append(res, AND, PEq(c))
		}
	}
	return res
}

type Upsert tabColsKey

func (ups *Upsert) Complete() error { return (*tabColsKey)(ups).complete() }

type tabColsKey struct {
	Table   TableDef
	Columns Columns
	Key     Columns
}

func (tck *tabColsKey) complete() error {
	if tck.Table == nil {
		if len(tck.Key) > 0 {
			tck.Table = tck.Key[0].Table()
		} else if len(tck.Columns) > 0 {
			tck.Table = tck.Columns[0].Table()
		} else {
			return errors.New("cannot determine table")
		}
	}
	if tck.Columns == nil {
		tck.Columns = Cols(tck.Table, tck.Key...)
	}
	return nil
}
