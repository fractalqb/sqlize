/*
Package bsq helps to Build Structured Queries for SQL in a maintainable
manner. Unlike many query builders out there it does not strive to formalize
SQL as Go DSL. Its primary focus it code maintainability when working with
SQL in your code. – Why do we need something different from the “usual”
approach?

I.e. consider the problem of keeping the Go code in line with the DB model to
be the primary maintenance task: It must be easy to find all parts of the
code that have to change when the DB model changes. Often, query builders
present an API that let's you do something like this:

	sql := Select("name", "age").From("users").Where(EqArg("id"))

And this might yield the expected string:

	SELECT name, age FROM users WHERE id=?

While this may help if you are not familiar with SQL, it doesn't do much.
What if the users table gets a column renamed, removed or even gets a
complely new column? How do you find all the relevant Go code that relate to
the users table?  At best a clever tool can search for all uses of the From()
method that gets the string "users" as an argument. Without such tool you
have to try a full text search.
Package bsq takes a different approach. This approach starts with a minimal
replica of the DB model made of Go objects:

	var tUsers = struct {
	    Table
	    ID, Name, Age Column
	}{
	    Table: Table{TableName: "users"},
	}

The extra effort for this replica helps much with the maintainability
problem. Consider this conceptual example (that omits some syntactic sugar
which actually would improve the code):

	query, _ := NewQuery(dialect,
		"SELECT ", &tUsers.Name, ", ", &tUsers.Age,
		" FROM ", &tUsers,
		" WHERE ", &tUsers.ID, '=', P("id"),
	)
	fmt.Println(query.String())

While this does not help with SQL syntax, one now can simply use standard
developer tools to locate all references to the 'tUsers' variable. And
renaming a column can be done at one single location where the Name column is
declared in the replica.

Package bsq builds on this concept to simplify the creation of SQL
statements. E.g. to get the example SELECT statement one would actually write
with bsq:

	q, _ := NewQuery(dialect, Select{
		Columns: ColsLs(&tUsers, &tUsers.ID), // all from tUsers without ID
		Key: Cols{&tUsers.ID},
	})
	sql := q.String()

# Defining a DB Schema in Go

TODO!

See also: dbtest.VerifyTables, dbtest.VerifyTable

# Writing Queries

TODO!
*/
package bsq
