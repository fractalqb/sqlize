package bsq

import (
	"context"
	"fmt"

	"git.fractalqb.de/fractalqb/sqlize"
)

type UpsertDialect interface {
	DefUpsert(defn Upsert) *Query
}

func DefUpsertQuery(d Dialect, ups Upsert) (*Query, error) {
	ud, ok := d.(UpsertDialect)
	if !ok {
		return nil, unsupportedFeature("upsert", d)
	}
	return ud.DefUpsert(ups), nil
}

func NewUpsertQuery(d Dialect, ups Upsert) (*Query, error) {
	q, err := DefUpsertQuery(d, ups)
	if err != nil {
		return nil, err
	}
	if err = q.Init(d); err != nil {
		return nil, err
	}
	return q, nil
}

type CreateDialect interface {
	DefCreate(idColumn *Column, cols ...*Column) *Query
	QueryCreateContext(ctx context.Context, db sqlize.Querier, q *Query, args ...any) (id int64, err error)
}

func DefCreateQuery(d Dialect, idColumn *Column, cols ...*Column) (*Query, error) {
	cd, ok := d.(CreateDialect)
	if !ok {
		return nil, unsupportedFeature("create", d)
	}
	return cd.DefCreate(idColumn, cols...), nil
}

func NewCreateQuery(d Dialect, idColumn *Column, cols ...*Column) (*Query, error) {
	q, err := DefCreateQuery(d, idColumn, cols...)
	if err != nil {
		return nil, err
	}
	if err = q.Init(d); err != nil {
		return nil, err
	}
	return q, nil
}

// TODO what about prepared queries
func QueryCreate(db sqlize.Querier, q *Query, args ...any) (id int64, err error) {
	return QueryCreateContext(context.Background(), db, q, args...)
}

func QueryCreateContext(ctx context.Context, db sqlize.Querier, q *Query, args ...any) (id int64, err error) {
	cd, ok := q.dialect.(CreateDialect)
	if !ok {
		return sqlize.NoID, unsupportedFeature("create", q.dialect)
	}
	return cd.QueryCreateContext(ctx, db, q, args...)
}

func unsupportedFeature(feature string, d Dialect) error {
	return fmt.Errorf("dialect %T does not support %s", d, feature)
}
