package bsq

import (
	"fmt"
	"reflect"

	_ "github.com/mattn/go-sqlite3"
)

func ExampleEachColumn() {
	type Entity struct {
		Table
		ID Column
	}
	tbl := struct {
		Entity
		Name Column
	}{
		Entity: Entity{
			Table: Table{Name: "yet_another_table"},
			ID:    Column{Name: "id"},
		},
		Name: Column{Name: "name"},
	}
	err := EachColumn(&tbl, func(c *Column, f *reflect.StructField) error {
		fmt.Println(c.Name, "->", f.Name)
		return nil
	})
	fmt.Println(err)
	// Output:
	// id -> ID
	// name -> Name
	// <nil>
}
