package bsq

type BindVars interface {
	Param(name string, col *Column) (string, error)
	ParamColumns() []*Column
}

func PositionalVars(sym string) func() BindVars {
	return func() BindVars { return &PositionBindVars{Symbol: sym} }
}

func IndexedVars(fmt string) func() BindVars {
	return func() BindVars { return NewIndexBindVars(fmt) }
}

func NamedVars(fmt string) func() BindVars {
	return func() BindVars { return NewNamedBindVars(fmt) }
}

func DefaultSQL(bv func() BindVars, quote map[string]string) Dialect {
	if bv == nil {
		bv = PositionalVars("?")
	}
	return dfltSQL{newBVars: bv, quote: quote}
}

type dfltSQL struct {
	newBVars func() BindVars
	quote    map[string]string
}

func (d dfltSQL) NewContext() DialectCtx { return &stdSQLCtx{BindVars: d.newBVars()} }

func (d dfltSQL) Quote(n string) string {
	if q, ok := d.quote[n]; ok {
		return q
	}
	return n
}

func (dfltSQL) Parameter(ctx DialectCtx, name string, col *Column) (string, error) {
	c := ctx.(*stdSQLCtx)
	return c.Param(name, col)
}

type stdSQLCtx struct {
	WriteCtx
	BindVars
}

func (ctx *stdSQLCtx) Close() {}
