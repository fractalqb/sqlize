package bsq

import (
	"fmt"

	"git.fractalqb.de/fractalqb/eloc/must"
)

var testTable = struct {
	Table
	ID, Name Column
}{Table: Table{Name: "thing"}}

func init() {
	err := InitSchema("", nil, &testTable)
	if err != nil {
		panic(err)
	}
}

func ExampleInsert() {
	ins := Insert{Table: &testTable} // Isert all columns of table

	q := must.Ret(NewQuery(testDialect, ins))
	fmt.Println(q.String())
	// Output:
	// INSERT INTO thing (ID, Name) VALUES (?, ?)
}

func ExampleSelect() {
	sel := Select{Table: &testTable} // Select * from table
	q := must.Ret(NewQuery(testDialect, sel))
	fmt.Println(q.String())

	sel = Select{Columns: Cols(&testTable)} // Select all columns from table
	q = must.Ret(NewQuery(testDialect, sel))
	fmt.Println(q.String())

	sel = Select{ // Select column(s) by key
		Columns: nil,
		Key:     Columns{&testTable.ID},
	}
	q = must.Ret(NewQuery(testDialect, sel))
	fmt.Println(q.String())

	sel = Select{ // Select column(s) by key
		Columns: Columns{&testTable.Name},
		Key:     Columns{&testTable.ID},
	}
	q = must.Ret(NewQuery(testDialect, sel))
	fmt.Println(q.String())

	sel = Select{ // Select column(s) by key
		Columns: Columns{&testTable.Name},
		Key:     Columns{&testTable.ID, &testTable.Name},
	}
	q = must.Ret(NewQuery(testDialect, sel))
	fmt.Println(q.String())
	// Output:
	// SELECT * FROM thing
	// SELECT ID, Name FROM thing
	// SELECT Name FROM thing WHERE ID=?
	// SELECT Name FROM thing WHERE ID=?
	// SELECT Name FROM thing WHERE ID=? AND Name=?
}

func ExampleUpdate() {
	upd := Update{Table: &testTable} // Update all columns
	q := must.Ret(NewQuery(DefaultSQL(IndexedVars("$%d"), nil), upd))
	fmt.Println(q.String())

	upd = Update{Key: Columns{&testTable.ID}} // Update all but key columns
	q = must.Ret(NewQuery(DefaultSQL(IndexedVars("$%d"), nil), upd))
	fmt.Println(q.String())

	upd = Update{
		Columns: Columns{&testTable.Name},
		Key:     Columns{&testTable.ID, &testTable.Name},
	}
	q = must.Ret(NewQuery(DefaultSQL(IndexedVars("$%d"), nil), upd))
	fmt.Println(q.String())
	// Output:
	// UPDATE thing SET ID=$1, Name=$2
	// UPDATE thing SET Name=$1 WHERE ID=$2
	// UPDATE thing SET Name=$1 WHERE ID=$2 AND Name=$1
}

func ExampleDelete() {
	q := must.Ret(NewQuery(testDialect, Delete{
		Key: Columns{&testTable.ID},
	}))
	fmt.Println(q.String())

	q = must.Ret(NewQuery(testDialect, Delete{
		Key: Columns{&testTable.ID, &testTable.Name},
	}))
	fmt.Println(q.String())
	// Output:
	// DELETE FROM thing WHERE ID=?
	// DELETE FROM thing WHERE ID=? AND Name=?
}
