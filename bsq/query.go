package bsq

import (
	"context"
	"database/sql"
	"fmt"
	"reflect"
	"strings"

	"git.fractalqb.de/fractalqb/sqlize"
)

type Query struct {
	defn    []any
	dialect Dialect
	tabs    []TableDef
	txt     strings.Builder
	pCols   []*Column
	oCols   []*Column
	stmt    *sql.Stmt
}

func DefQuery(defn ...any) *Query { return &Query{defn: defn} }

// NewQuery first checks once if defn has only one element with a Defn() []any
// method. If so, it uses the result from Defn() to replace defn. Then it creates
// and Inits a new Query with Dialect=d and Defn=defn.
func NewQuery(d Dialect, defn ...any) (*Query, error) {
	q := DefQuery(defn...)
	if err := q.Init(d); err != nil {
		return nil, err
	}
	return q, nil
}

func NewQueryPrep(db *sql.DB, d Dialect, defn ...any) (*Query, error) {
	q, err := NewQuery(d, defn...)
	if err != nil {
		return q, err
	}
	if err = q.Prepare(db); err != nil {
		return nil, err
	}
	return q, nil
}

func NewQueryPrepContext(ctx context.Context, db *sql.DB, d Dialect, defn ...any) (*Query, error) {
	q, err := NewQuery(d, defn...)
	if err != nil {
		return q, err
	}
	if err = q.PrepareContext(ctx, db); err != nil {
		return nil, err
	}
	return q, nil
}

// Init initializes the Query according to its definition. If d is not nil it
// will initialize q.dialect. Trying to override an already set dialect results
// in an error. Init must not be called before InitSchema for the tables used in
// the query. One may use Queries to initialize a set of global Query variables.
func (q *Query) Init(d Dialect) error {
	q.expandDefn()
	if err := q.scan(q.defn); err != nil {
		return err
	}
	q.dialect = d
	dctx := q.dialect.NewContext()
	defer dctx.Close()
	if err := q.write(dctx, q.defn); err != nil {
		return err
	}
	q.pCols = dctx.ParamColumns()
	q.oCols = dctx.OutColumns()
	return nil
}

func (q *Query) expandDefn() {
	if len(q.defn) == 1 {
		if tmp, ok := q.defn[0].(interface{ Defn() []any }); ok {
			q.defn = tmp.Defn()
		}
	}
}

func (q *Query) Prepare(db *sql.DB) (err error) {
	q.stmt, err = db.Prepare(q.String())
	return err
}

func (q *Query) PrepareContext(ctx context.Context, db *sql.DB) (err error) {
	q.stmt, err = db.PrepareContext(ctx, q.String())
	return err
}

type Prunable int

const (
	PruneDefn Prunable = (1 << iota)
	PruneTabs
	PruneString
)

func (q *Query) Prune(p Prunable) {
	if p&PruneDefn == PruneDefn {
		q.defn = nil
	}
	if p&PruneTabs == PruneTabs {
		q.tabs = nil
	}
	if p&PruneString == PruneString {
		q.txt.Reset()
	}
}

func (q *Query) String() string { return q.txt.String() }

func (q *Query) Stmt() *sql.Stmt { return q.stmt }

func (q *Query) Dialect() Dialect { return q.dialect }

func (q *Query) ParamCols() []*Column { return q.pCols }

func (q *Query) OutCols() []*Column { return q.oCols }

func ColsOf(t TableDef, without ...*Column) (cols []any) {
	EachColumn(t, func(c *Column, _ *reflect.StructField) error {
		for _, not := range without {
			if c == not {
				return nil
			}
		}
		cols = append(cols, c)
		return nil
	})
	return
}

func (q *Query) Exec(db sqlize.Querier, args ...any) (sql.Result, error) {
	switch db := db.(type) {
	case *sql.Tx:
		if q.stmt == nil {
			return db.Exec(q.String(), args...)
		}
		return db.Stmt(q.stmt).Exec(args...)
	case *sql.DB:
		return db.Exec(q.String(), args...)
	}
	return q.stmt.Exec(args...)
}

func (q *Query) ExecContext(ctx context.Context, db sqlize.Querier, args ...any) (sql.Result, error) {
	switch db := db.(type) {
	case *sql.Tx:
		if q.stmt == nil {
			return db.ExecContext(ctx, q.String(), args...)
		}
		return db.Stmt(q.stmt).ExecContext(ctx, args...)
	case *sql.DB:
		return db.ExecContext(ctx, q.String(), args...)
	}
	return q.stmt.ExecContext(ctx, args...)
}

func (q *Query) Query(db sqlize.Querier, args ...any) (*sql.Rows, error) {
	switch db := db.(type) {
	case *sql.Tx:
		if q.stmt == nil {
			return db.Query(q.String(), args...)
		}
		return db.Stmt(q.stmt).Query(args...)
	case *sql.DB:
		return db.Query(q.String(), args...)
	}
	return q.stmt.Query(args...)
}

func (q *Query) QueryContext(ctx context.Context, db sqlize.Querier, args ...any) (*sql.Rows, error) {
	switch db := db.(type) {
	case *sql.Tx:
		if q.stmt == nil {
			return db.QueryContext(ctx, q.String(), args...)
		}
		return db.Stmt(q.stmt).QueryContext(ctx, args...)
	case *sql.DB:
		return db.QueryContext(ctx, q.String(), args...)
	}
	return q.stmt.QueryContext(ctx, args...)
}

func (q *Query) QueryRow(db sqlize.Querier, args ...any) *sql.Row {
	switch db := db.(type) {
	case *sql.Tx:
		if q.stmt == nil {
			return db.QueryRow(q.String(), args...)
		}
		return db.Stmt(q.stmt).QueryRow(args...)
	case *sql.DB:
		return db.QueryRow(q.String(), args...)
	}
	return q.stmt.QueryRow(args...)
}

func (q *Query) QueryRowContext(ctx context.Context, db sqlize.Querier, args ...any) *sql.Row {
	switch db := db.(type) {
	case *sql.Tx:
		if q.stmt == nil {
			return db.QueryRowContext(ctx, q.String(), args...)
		}
		return db.Stmt(q.stmt).QueryRowContext(ctx, args...)
	case *sql.DB:
		return db.QueryRowContext(ctx, q.String(), args...)
	}
	return q.stmt.QueryRowContext(ctx, args...)
}

// P adds a parameter to a query definition. The parameter key must either be a
// string or a ColumnRef to identify the parameter. The key lets the query
// generator recognize the same parameter when used several times in a query.
func P(key any) any { return param{key} }

func ColP(sql ...any) any { return colParam(sql) }

// PEq adds 'column-name'='bind-var' to the query
func PEq(col ColumnRef) any { return colParam{col, '=', P(col)} }

// PNe adds 'column-name'<>'bind-var' to the query
func PNe(col ColumnRef) any { return colParam{col, "<>", P(col)} }

// PLt adds 'column-name'<'bind-var' to the query
func PLt(col ColumnRef) any { return colParam{col, '<', P(col)} }

// PLe adds 'column-name'<='bind-var' to the query
func PLe(col ColumnRef) any { return colParam{col, "<=", P(col)} }

// PGt adds 'column-name'>'bind-var' to the query
func PGt(col ColumnRef) any { return colParam{col, '>', P(col)} }

// PGe adds 'column-name'>='bind-var' to the query
func PGe(col ColumnRef) any { return colParam{col, ">=", P(col)} }

func Out(sql ...any) any { return out(sql) }

type List struct {
	Elems          []any
	Pre, Sep, Post string
}

func Ls(def ...string) List {
	switch len(def) {
	case 0:
		return List{}
	case 1:
		return List{Sep: def[0]}
	case 2:
		return List{Pre: def[0], Post: def[1]}
	case 3:
		return List{Pre: def[0], Sep: def[1], Post: def[2]}
	}
	elems := make([]any, len(def)-3)
	for i, e := range def[3:] {
		elems[i] = e
	}
	return List{
		Elems: elems,
		Pre:   def[0], Sep: def[1], Post: def[2],
	}
}

func (ls List) Of(elems ...any) List {
	return List{Elems: elems, Pre: ls.Pre, Sep: ls.Sep, Post: ls.Post}
}

func (l List) write(dctx DialectCtx, q *Query) error {
	if l.Pre != "" {
		q.txt.WriteString(l.Pre)
	}
	if len(l.Elems) > 0 {
		if err := q.write(dctx, l.Elems[0]); err != nil {
			return err
		}
		for _, e := range l.Elems[1:] {
			q.txt.WriteString(l.Sep)
			if err := q.write(dctx, e); err != nil {
				return err
			}
		}
	}
	if l.Post != "" {
		q.txt.WriteString(l.Post)
	}
	return nil
}

func (l List) scan(q *Query) error {
	for _, e := range l.Elems {
		if err := q.scan(e); err != nil {
			return err
		}
	}
	return nil
}

func JoinEq(from, to ColumnRef) any { return eqJoin{from: from, to: to} }

func LeftJoinEq(from, to ColumnRef) any {
	return eqJoin{bias: "LEFT", from: from, to: to}
}

func RightJoinEq(from, to ColumnRef) any {
	return eqJoin{bias: "RIGHT", from: from, to: to}
}

func FullJoinEq(from, to ColumnRef) any {
	return eqJoin{bias: "FULL", from: from, to: to}
}

type WriteCtx struct {
	pCol  ColumnRef
	oCols []*Column
}

func (c *WriteCtx) WriteContext() *WriteCtx { return c }

func (c *WriteCtx) OutColumns() []*Column { return c.oCols }

func (c *WriteCtx) addOut(o any) {
	if col, ok := o.(ColumnRef); ok {
		c.oCols = append(c.oCols, col.ColumnRef())
	} else {
		c.oCols = append(c.oCols, nil)
	}
}

type qScanner interface{ scan(*Query) error }

type qWriter interface {
	write(DialectCtx, *Query) error
}

type param struct{ key any }

func (p param) write(dctx DialectCtx, q *Query) (err error) {
	var txt string
	switch key := p.key.(type) {
	case string:
		if txt, err = q.dialect.Parameter(dctx, key, nil); err != nil {
			return err
		}
	case ColumnRef:
		pcol := dctx.WriteContext().pCol
		if pcol == nil {
			if txt, err = q.dialect.Parameter(dctx, key.AName(), key.ColumnRef()); err != nil {
				return err
			}
		} else if pcol == key {
			if txt, err = q.dialect.Parameter(dctx, key.AName(), key.ColumnRef()); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("column %s in parameter column %s context",
				key.AName(),
				pcol.AName(),
			)
		}
	default:
		return fmt.Errorf("invalid %[1]T parameter key '%+[1]v'", key)
	}
	q.txt.WriteString(txt)
	return nil
}

type colParam []any

func (cp colParam) write(dctx DialectCtx, q *Query) error {
	wctx := dctx.WriteContext()
	if wctx.pCol != nil {
		return fmt.Errorf("nesting column parameter to %s", wctx.pCol.QName())
	}
	for _, e := range cp {
		if c, ok := e.(ColumnRef); ok {
			if wctx.pCol != nil {
				return fmt.Errorf("multiple column %s and %s in column parameters",
					wctx.pCol.QName(),
					c.QName(),
				)
			}
			wctx.pCol = c
			defer func() { wctx.pCol = nil }()
		}
	}
	for _, e := range cp {
		if err := q.write(dctx, e); err != nil {
			return err
		}
	}
	return nil
}

type out []any

func (o out) write(dctx DialectCtx, q *Query) error {
	wctx := dctx.WriteContext()
	if len(o) > 0 {
		if err := q.write(dctx, o[0]); err != nil {
			return err
		}
		wctx.addOut(o[0])
		for _, e := range o[1:] {
			q.txt.WriteString(", ")
			if err := q.write(dctx, e); err != nil {
				return err
			}
			wctx.addOut(e)
		}
	}
	return nil
}

func (o out) scan(q *Query) error {
	for _, e := range o {
		if err := q.scan(e); err != nil {
			return err
		}
	}
	return nil
}

type eqJoin struct {
	bias     string
	from, to ColumnRef
}

func (j eqJoin) write(d DialectCtx, q *Query) error {
	if j.bias != "" {
		q.txt.WriteByte(' ')
		q.txt.WriteString(j.bias)
	}
	q.txt.WriteString(" JOIN ")
	q.write(d, j.to.Table())
	q.txt.WriteString(" ON (")
	q.txt.WriteString(j.from.AName())
	q.txt.WriteRune('=')
	q.txt.WriteString(j.to.AName())
	q.txt.WriteRune(')')
	return nil
}

func (j eqJoin) scan(q *Query) error {
	if err := q.scan(j.from); err != nil {
		return err
	}
	return q.scan(j.to)
}

func (q *Query) write(dctx DialectCtx, sql any) error {
	switch sql := sql.(type) {
	case TableDef:
		if len(q.tabs) > 1 {
			fmt.Fprintf(&q.txt, "%s %s", sql.TableDef().QName(), sql.Alias())
		} else {
			q.txt.WriteString(sql.TableDef().QName())
		}
		return nil
	case ColumnRef:
		if len(q.tabs) > 1 {
			q.txt.WriteString(q.Dialect().Quote(sql.AName()))
		} else {
			q.txt.WriteString(q.Dialect().Quote(sql.ColumnRef().Name))
		}
		return nil
	case qWriter:
		return sql.write(dctx, q)
	case rune:
		q.txt.WriteRune(sql)
		return nil
	}
	v := reflect.ValueOf(sql)
	switch v.Kind() {
	case reflect.Slice, reflect.Array:
		for i := 0; i < v.Len(); i++ {
			if err := q.write(dctx, v.Index(i).Interface()); err != nil {
				return err
			}
		}
	default:
		fmt.Fprint(&q.txt, sql)
	}
	return nil
}

func (q *Query) scan(sql any) error {
	switch sql := sql.(type) {
	case qScanner:
		return sql.scan(q)
	case TableDef:
		return q.foundTable(sql)
	case ColumnRef:
		return q.foundTable(sql.Table())
	}
	v := reflect.ValueOf(sql)
	switch v.Kind() {
	case reflect.Slice, reflect.Array:
		for i := 0; i < v.Len(); i++ {
			if err := q.scan(v.Index(i).Interface()); err != nil {
				return err
			}
		}
	}
	return nil
}

func (q *Query) foundTable(t TableDef) error {
	for _, know := range q.tabs {
		if t == know {
			return nil
		}
	}
	if len(q.tabs) > 0 {
		if len(q.tabs) == 1 {
			if t0 := q.tabs[0]; t0.Alias() == "" {
				return fmt.Errorf("table %s without alias used in complext query",
					t0.TableDef().QName(),
				)
			}
		}
		if t.Alias() == "" {
			return fmt.Errorf("table %s without alias used in complext query",
				t.TableDef().QName(),
			)
		}
	}
	q.tabs = append(q.tabs, t)
	return nil
}
