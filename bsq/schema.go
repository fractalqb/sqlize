package bsq

import (
	"fmt"
	"reflect"
	"sync"
)

type TableDef interface {
	TableDef() *Table
	Alias() string
}

type ColumnRef interface {
	ColumnRef() *Column
	Table() TableDef
	QName() string
	AName() string
}

// Table is used to declare the table data for the DB model replica. Replica are
// used to define SQL queries.
//
// To see how to use it, read example of EachColumn and InitSchema.
type Table struct {

	// The schema name of the declared table. It can also be set with
	// InitSchema.
	Schema string

	// The table name.
	Name string

	// A default alias for SQL queries. Specific aliases can be created with the
	// Alias method.
	DefaultAlias string

	next TableDef
}

func (t *Table) TableDef() *Table { return t }

func (t *Table) Alias() string { return t.DefaultAlias }

func (t *Table) QName() string {
	if t.Schema == "" {
		return t.Name
	}
	return t.Schema + "." + t.Name
}

// As creates a new alias of the table.
func (t *Table) As(alias string) TableDef { return tAlias{t, alias} }

type Column struct {
	Name string

	tbl TableDef
	idx int
}

func (c *Column) Table() TableDef { return c.tbl }

func (c *Column) Index() int { return c.idx }

// QName returns the column's name qualified with its table's name. If
// qualifyTable is ture the table name itself will be qualified as well.
func (c *Column) QName() string {
	t := c.Table()
	return t.TableDef().QName() + "." + c.Name
}

func (c *Column) AName() string {
	t := c.Table()
	a := t.Alias()
	if a == "" {
		return t.TableDef().QName() + "." + c.Name
	}
	return a + "." + c.Name
}

// In panics if c's table is different from t
func (c *Column) In(t TableDef) ColumnRef {
	ct, tt := c.tbl.TableDef(), t.TableDef()
	if ct != tt {
		panic(fmt.Errorf("column %s cannot be qualified with %s",
			c.QName(),
			t.TableDef().QName(),
		))
	}
	return cRef{c, t}
}

func (c *Column) ColumnRef() *Column { return c }

type InitOpts struct {
	// If true InitSchema will set the passed schema on all tables. Otherwise,
	// the schema will be set only if the table does not yet have a schema name.
	ForceSchema bool

	// If true InitSchema will return an error if a table does not have a
	// default alias.
	MustAlias bool

	// Used to set column names from struct field names. This is only used on
	// columns that do not yet have a name set. If ColNameMap is nil, the struct
	// filed name will be used as is.
	ColNameMap func(fieldname string) (columname string)
}

// InitSchema adds the tables to the list of known tables. It does not check
// for duplicates because there may be reasonable applications that hold table
// definitions for equally named tables from different databases. The schema
// name of each table is set according to opts.ForceSchema and the default alias
// of each table is checked according to opts.MustAlias.
func InitSchema(schema string, opts *InitOpts, tables ...TableDef) error {
	if opts == nil {
		opts = &InitOpts{}
	}
	if opts.ColNameMap == nil {
		opts.ColNameMap = func(n string) string { return n }
	}
	alias := make(map[string]*Table)
	tableDefLock.Lock()
	defer tableDefLock.Unlock()
	for t := tableDefList; t != nil; {
		td := t.TableDef()
		alias[td.DefaultAlias] = td
		t = td.next
	}
	for _, t := range tables {
		td := t.TableDef()
		if opts.ForceSchema || td.Schema == "" {
			td.Schema = schema
		}
		if td.DefaultAlias != "" {
			if ot := alias[td.DefaultAlias]; ot != nil {
				return fmt.Errorf("default alias '%s' on %s and %s",
					td.DefaultAlias,
					ot.QName(),
					td.QName(),
				)
			}
		} else if opts.MustAlias {
			return fmt.Errorf("table %s without default alias", td.QName())
		}
		colIdx := 0
		err := EachColumn(t, func(c *Column, f *reflect.StructField) error {
			c.tbl = t
			c.idx = colIdx
			colIdx++
			if c.Name == "" {
				c.Name = opts.ColNameMap(f.Name)
			}
			return nil
		})
		if err != nil {
			return err
		}
		td.next = tableDefList
		tableDefList = t
	}
	return nil
}

func EachTable(do func(TableDef) error) error {
	tableDefLock.RLock()
	defer tableDefLock.RUnlock()
	for it := tableDefList; it != nil; it = it.TableDef().next {
		if err := do(it); err != nil {
			return err
		}
	}
	return nil
}

func EachColumn(def TableDef, do func(*Column, *reflect.StructField) error) error {
	defval := reflect.Indirect(reflect.ValueOf(def))
	if defval.Kind() != reflect.Struct {
		return fmt.Errorf("cannot visit columns in %s", defval.Type().Name())
	}
	return eachCol(defval, do)
}

type Columns []*Column

// Cols returns all columns of t except the columns in 'without'.
func Cols(t TableDef, without ...*Column) (cols Columns) {
	EachColumn(t, func(c *Column, _ *reflect.StructField) error {
		for _, not := range without {
			if c == not {
				return nil
			}
		}
		cols = append(cols, c)
		return nil
	})
	return
}

func (cs Columns) Defn(add []any) []any {
	if add == nil {
		add = make([]any, 0, len(cs))
	}
	for _, c := range cs {
		add = append(add, c)
	}
	return add
}

func eachCol(defval reflect.Value, do func(*Column, *reflect.StructField) error) error {
	if defval.Kind() != reflect.Struct {
		return nil
	}
	defty := defval.Type()
	for i := 0; i < defval.NumField(); i++ {
		field := defty.Field(i)
		if !field.IsExported() {
			continue
		}
		fval := defval.Field(i)
		if !fval.CanInterface() {
			continue
		}
		switch fval.Interface().(type) {
		case Table:
		case Column:
			col := fval.Addr().Interface().(*Column)
			if err := do(col, &field); err != nil {
				return err
			}
		default:
			if field.Anonymous && field.IsExported() && field.Type.Kind() == reflect.Struct {
				if err := eachCol(fval, do); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

var (
	tableDefList TableDef
	tableDefLock sync.RWMutex
)

type tAlias struct {
	t *Table
	a string
}

func (a tAlias) TableDef() *Table { return a.t }

func (a tAlias) Alias() string { return a.a }

type cRef struct {
	*Column
	t TableDef
}

func (r cRef) Table() TableDef { return r.t }

func (r cRef) QName() string {
	return r.t.TableDef().QName() + "." + r.ColumnRef().Name
}

func (r cRef) AName() string {
	a := r.t.Alias()
	if a == "" {
		return r.QName()
	}
	return a + "." + r.ColumnRef().Name
}
