package bsq

import (
	"database/sql"
	"fmt"
	"sync"
)

// Queries is used to collect a set of queries and later apply an operation to
// all queries in the set - typically to initialise and/or prepare them. A zero
// value is valid to start collecting queries.
type Queries struct {
	qs    []*Query
	sub   []*Queries
	mutex sync.Mutex
}

// NewQueries is used to hierachically create a collection of queries. The
// sub-collections are passed as arguemnts. Then the new Queries object can be
// used to add more queries.
func NewQueries(sub ...*Queries) *Queries {
	return &Queries{sub: sub}
}

// Add adds a queriy to the collection. Add is safe for concurrent use, also
// with ForEach.
func (qs *Queries) Add(q *Query) *Query {
	qs.mutex.Lock()
	defer qs.mutex.Unlock()
	qs.qs = append(qs.qs, q)
	return q
}

// MustAdd panics with err if err is not nil. Otherwise it calls qs.Add(q).
func (qs *Queries) MustAdd(q *Query, err error) *Query {
	if err != nil {
		panic(err)
	}
	return qs.Add(q)
}

func (qs *Queries) ForEach(clear bool, do func(*Query) error) error {
	var qls []*Query
	var sls []*Queries
	qs.mutex.Lock()
	qls, sls = qs.qs, qs.sub
	if clear {
		qs.qs, qs.sub = nil, nil
	}
	qs.mutex.Unlock()
	for _, s := range sls {
		err := s.ForEach(clear, do)
		if err != nil {
			return err
		}
	}
	for i, q := range qls {
		err := do(q)
		if err != nil {
			return fmt.Errorf("query %d: %w", i, err)
		}
	}
	return nil
}

func (qs *Queries) Init(clear bool, d Dialect) error {
	return qs.ForEach(clear, func(q *Query) error { return q.Init(d) })
}

func (qs *Queries) Prepare(clear bool, db *sql.DB, initWith Dialect) error {
	if initWith == nil {
		return qs.ForEach(clear, func(q *Query) error {
			return q.Prepare(db)
		})
	}
	return qs.ForEach(clear, func(q *Query) (err error) {
		if err = q.Init(initWith); err != nil {
			return err
		}
		return q.Prepare(db)
	})
}
