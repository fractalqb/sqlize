// Package keywords defines some handy SQL keywords as const strings
// for use with the bsq query builder. This package is indendet to be
// imported unqualified, i.e. as '.', into the using source file.
package keywords

const (
	SELECT      = "SELECT "
	DELETE      = "DELETE "
	FROM        = " FROM "
	JOIN        = " JOIN "
	ON          = " ON ("
	LEFT_JOIN   = " LEFT JOIN "
	RIGHT_JOIN  = " RIGHT JOIN "
	FULL_JOIN   = " FULL JOIN "
	UPDATE      = "UPDATE "
	SET         = " SET "
	WHERE       = " WHERE "
	AND         = " AND "
	OR          = " OR "
	IS_NOT_NULL = " IS NOT NULL"
	IS_NULL     = " IS NULL"
	ORDER_BY    = " ORDER BY "
	GROUP_BY    = " GROUP BY "
	DELETE_FROM = "DELETE FROM "
	INSERT_INTO = "INSERT INTO "
	VALUES      = " VALUES "
	LIMIT       = " LIMIT "
	BETWEEN     = " BETWEEN "
)
