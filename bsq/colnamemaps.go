package bsq

import (
	"strings"
	"unicode"
)

func DefaultNameMap() func(string) string { return dfltMap.Map }

func CamelToSnake(name string) string {
	var sb strings.Builder
	for i, r := range name {
		if i > 0 && unicode.IsUpper(r) {
			sb.WriteByte('_')
		}
		sb.WriteRune(unicode.ToLower(r))
	}
	return sb.String()
}

type ExplicitMap struct {
	explicit map[string]string
	fallback func(string) string
}

func NewExplicitMap(mapping map[string]string, fallback func(string) string) *ExplicitMap {
	return &ExplicitMap{mapping, fallback}
}

func (m ExplicitMap) Map(name string) string {
	if mapped, ok := m.explicit[name]; ok {
		return mapped
	}
	return m.fallback(name)
}

func (m ExplicitMap) With(mapping map[string]string) ExplicitMap {
	tmp := ExplicitMap{make(map[string]string), m.fallback}
	for k, v := range m.explicit {
		tmp.explicit[k] = v
	}
	for k, v := range mapping {
		if v == "" {
			delete(tmp.explicit, k)
		} else {
			tmp.explicit[k] = v
		}
	}
	return tmp
}

var dfltMap = ExplicitMap{
	explicit: map[string]string{
		"ID": "id",
	},
	fallback: CamelToSnake,
}
