package examples

import (
	"log"
	"os"

	"git.fractalqb.de/fractalqb/sqlize"
	"github.com/mattn/go-sqlite3"
)

// use logging DB driver to see whats going on
const Driver = "sqlize-exampledriver"

func init() {

	sqlize.LogDriver(
		Driver, &sqlite3.SQLiteDriver{},
		sqlize.NewStdLogger(log.New(os.Stdout, "DB ", log.Lmicroseconds)),
		sqlize.LogAll,
	)
}
