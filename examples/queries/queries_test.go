package main

import (
	"fmt"
)

func Example_qUserInsert() {
	fmt.Println(qUserInsert.String())
	// Output:
	// INSERT INTO users (id, name, reg_date) VALUES (?, ?, ?)
}

func Example_qUserRead() {
	fmt.Println(qUserRead.String())
	// Output:
	// SELECT name, reg_date FROM users WHERE id=?
}
