package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	elocmust "git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/sqlize"
	"git.fractalqb.de/fractalqb/sqlize/bsq"
	"git.fractalqb.de/fractalqb/sqlize/examples"
)

var (
	dialect = bsq.DefaultSQL(nil, nil)

	qUserInsert = mustRet(bsq.NewQuery(
		dialect,
		bsq.Insert{Table: &examples.TUsers},
	))

	qUserRead = mustRet(bsq.NewQuery(
		dialect,
		bsq.Select{Key: bsq.Columns{&examples.TUsers.ID}},
	))
)

const dbFile = "db.sqlite3"

func main() {
	// remove DB file to start with a fresh DB for this demo – normally you keep it
	if err := os.Remove(dbFile); err != nil && !os.IsNotExist(err) {
		log.Fatal(err)
	}

	db := mustRet(sql.Open(examples.Driver, dbFile))
	defer db.Close()

	// run the create script to setup the DB
	must(examples.RunCreateScript(db, false))

	// add some rows as a single transaction
	must(sqlize.Xaction(db, func(tx *sql.Tx) (err error) {
		defer elocmust.RecoverAs(&err)
		t := time.Now()
		mustRet(db.Exec(qUserInsert.String(), 1, "foo", t))
		mustRet(db.Exec(qUserInsert.String(), 2, "bar", t))
		mustRet(db.Exec(qUserInsert.String(), 3, "baz", t))
		return
	}))

	// let's see if we can find user with ID=2
	var name, regDateStr string
	must(db.QueryRow(qUserRead.String(), 2).Scan(&name, &regDateStr))
	fmt.Println("User 2:", name, regDateStr)
}

func must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func mustRet[T any](v T, err error) T {
	if err != nil {
		log.Fatal(err)
	}
	return v
}
