package examples

// Create and initialize a schema that is used in the examples

import (
	_ "embed"
	"strings"

	"git.fractalqb.de/fractalqb/sqlize"
	"git.fractalqb.de/fractalqb/sqlize/bsq"
)

//go:embed create.sql
var createScript string

//go:embed drop.sql
var dropScript string

func RunCreateScript(q sqlize.Querier, drop bool) error {
	sqlize.RunScript(q, strings.NewReader(dropScript), true)
	return sqlize.RunScript(q, strings.NewReader(createScript), false)
}

func init() {
	bsq.InitSchema(
		"",
		&bsq.InitOpts{ColNameMap: bsq.DefaultNameMap()},
		&TUsers,
		&TGroups,
		&TMembership,
	)
}

var TUsers = struct {
	bsq.Table
	ID, Name, RegDate bsq.Column
}{
	Table: bsq.Table{Name: "users"},
}

var TGroups = struct {
	bsq.Table
	ID, Name bsq.Column
}{
	Table: bsq.Table{Name: "groups"},
}

var TMembership = struct {
	bsq.Table
	User, Group bsq.Column
}{
	Table: bsq.Table{Name: "membership"},
}
