package examples

import (
	"time"

	"git.fractalqb.de/fractalqb/sqlize/bsq"
	"git.fractalqb.de/fractalqb/sqlize/entity"
	"git.fractalqb.de/fractalqb/sqlize/null"
)

type User struct {
	entity.Base[entity.ID32]
	Name    string
	RegDate time.Time
}

func (u *User) Equal(v *User) bool {
	if u == nil {
		return v == nil
	}
	if v == nil {
		return false
	}
	return u.Name == v.Name && u.RegDate.Equal(v.RegDate)
}

func NewUserDBRepo(d bsq.Dialect) (entity.Repo[User, entity.ID32], error) {
	return entity.NewDBRepo(d, &TUsers.ID, &UserFields, nil)
}

var UserFields entity.FieldMap[User]

func init() {
	entity.MapIDField(&UserFields, &TUsers.ID)
	UserFields.MapToAndFrom(&TUsers.Name,
		func(u *User, _ any) any {
			if u.Name == "" {
				return nil
			} else {
				return u.Name
			}
		},
		func(u *User, _ any) any { return &u.Name },
	)
	UserFields.MapBoth(&TUsers.RegDate, func(u *User, _ any) any {
		return null.Time{P: &u.RegDate}
	})
}
