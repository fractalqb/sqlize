package examples

import (
	"database/sql"
	"testing"

	"git.fractalqb.de/fractalqb/sqlize"
	"git.fractalqb.de/fractalqb/sqlize/dbtest"
	_ "github.com/mattn/go-sqlite3"
)

func TestSchema(t *testing.T) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()
	if err = sqlize.RunScriptFile(db, "create.sql", false); err != nil {
		t.Fatal(err)
	}
	if !dbtest.VerifyTables(t, db, nil) {
		t.Error("sqlize schema mismatches database")
	}
}
