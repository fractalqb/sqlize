package sqlize

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"errors"
	"fmt"
	"testing"

	"github.com/mattn/go-sqlite3"
	"github.com/ngrok/sqlmw"
)

func ExampleCommitOrRollbackOn() {
	sql.Register("CommitOrRollbackOn", sqlmw.Driver(&sqlite3.SQLiteDriver{}, new(printCommitNRollbackstruct)))
	db, _ := sql.Open("CommitOrRollbackOn", ":memory:")
	defer db.Close()

	tx, ok, err := Begin(db)
	if err != nil {
		fmt.Println(err)
		return
	}
	if ok {
		defer CommitOrRollbackOn(&err, tx)
	}
	// An non-nil err will rool back
	err = errors.New("Just an error")

	// Output:
	// TxRollback
}

func ExampleXaction() {
	sql.Register("ExampleXaction", sqlmw.Driver(&sqlite3.SQLiteDriver{}, new(printCommitNRollbackstruct)))
	db, _ := sql.Open("ExampleXaction", ":memory:")
	defer db.Close()

	Xaction(db, func(tx *sql.Tx) error {
		return errors.New("Just an error")
	})

	// Output:
	// TxRollback
}

func TestXaction(t *testing.T) {
	t.Run("BeginTx error", func(t *testing.T) {
		sql.Register(t.Name(), sqlmw.Driver(&sqlite3.SQLiteDriver{}, new(beginTxError)))
		db, err := sql.Open(t.Name(), ":memory:")
		if err != nil {
			t.Fatal(err)
		}
		defer db.Close()
		err = Xaction(db, func(*sql.Tx) error { return errors.New("do-error") })
		if err == nil {
			t.Fatal("no error detected")
		}
		if e := err.Error(); e != "cannot begin Tx" {
			t.Error(e)
		}
	})
	t.Run("TxCommit error", func(t *testing.T) {
		sql.Register(t.Name(), sqlmw.Driver(&sqlite3.SQLiteDriver{}, new(txCommitError)))
		db, err := sql.Open(t.Name(), ":memory:")
		if err != nil {
			t.Fatal(err)
		}
		defer db.Close()
		err = Xaction(db, func(*sql.Tx) error { return nil })
		if err == nil {
			t.Fatal("no error detected")
		}
		if e := err.Error(); e != "cannot commit Tx" {
			t.Error(e)
		}
	})
	t.Run("TxRollback error", func(t *testing.T) {
		sql.Register(t.Name(), sqlmw.Driver(&sqlite3.SQLiteDriver{}, new(txRollbackError)))
		db, err := sql.Open(t.Name(), ":memory:")
		if err != nil {
			t.Fatal(err)
		}
		defer db.Close()
		err = Xaction(db, func(*sql.Tx) error { return errors.New("do-error") })
		if err == nil {
			t.Fatal("no error detected")
		}
		if e := err.Error(); e != "rollback for 'do-error' fails with: cannot rollback Tx" {
			t.Error(e)
		}
	})
}

type printCommitNRollbackstruct struct{ sqlmw.NullInterceptor }

func (*printCommitNRollbackstruct) TxCommit(ctx context.Context, tx driver.Tx) error {
	fmt.Println("TxCommit")
	return nil
}

func (*printCommitNRollbackstruct) TxRollback(ctx context.Context, tx driver.Tx) error {
	fmt.Println("TxRollback")
	return nil
}

type beginTxError struct{ sqlmw.NullInterceptor }

func (*beginTxError) ConnBeginTx(
	ctx context.Context,
	_ driver.ConnBeginTx,
	_ driver.TxOptions,
) (context.Context, driver.Tx, error) {
	return ctx, nil, errors.New("cannot begin Tx")
}

type txCommitError struct{ sqlmw.NullInterceptor }

func (*txCommitError) TxCommit(ctx context.Context, tx driver.Tx) error {
	return errors.New("cannot commit Tx")
}

type txRollbackError struct{ sqlmw.NullInterceptor }

func (*txRollbackError) TxRollback(ctx context.Context, tx driver.Tx) error {
	return errors.New("cannot rollback Tx")
}
