package sqlize

import (
	"bufio"
	"fmt"
	"strings"
)

func Example_splitPara() {
	rd := strings.NewReader(`1 one line
1 next line
	
2 another line
   

3 third 1
3 third 2`)
	scn := bufio.NewScanner(rd)
	scn.Split(splitPara)
	for scn.Scan() {
		fmt.Print(scn.Text())
		fmt.Println("¶")
	}
	// Output:
	// 1 one line
	// 1 next line¶
	// 2 another line¶
	// 3 third 1
	// 3 third 2¶
}
