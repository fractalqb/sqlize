module git.fractalqb.de/fractalqb/sqlize

go 1.22.0

toolchain go1.23.1

require (
	git.fractalqb.de/fractalqb/eloc v0.3.0
	git.fractalqb.de/fractalqb/testerr v0.1.1
	github.com/jackc/pgx/v5 v5.7.2
	github.com/mattn/go-sqlite3 v1.14.24
	github.com/ngrok/sqlmw v0.0.0-20220520173518-97c9c04efc79
	golang.org/x/exp v0.0.0-20250106191152-7588d65b2ba8
)

require (
	git.fractalqb.de/fractalqb/daq v0.11.0 // indirect
	github.com/awnumar/memcall v0.4.0 // indirect
	github.com/awnumar/memguard v0.22.5 // indirect
	github.com/rogpeppe/go-internal v1.13.1 // indirect
	golang.org/x/sys v0.29.0 // indirect
)

require (
	git.fractalqb.de/fractalqb/nmconv v1.0.2
	git.fractalqb.de/fractalqb/yacfg v0.7.1
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20240606120523-5a60cdf6a761 // indirect
	github.com/jackc/puddle/v2 v2.2.2 // indirect
	github.com/jimsmart/schema v0.2.1
	golang.org/x/crypto v0.32.0 // indirect
	golang.org/x/sync v0.10.0 // indirect
	golang.org/x/term v0.28.0
	golang.org/x/text v0.21.0 // indirect
)
