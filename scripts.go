package sqlize

import (
	"bufio"
	"io"
	"os"
	"regexp"
	"strings"
)

var splitPattern = regexp.MustCompile("\r?\n([ \t]*\r?\n){1,}")

func splitPara(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	match := splitPattern.FindIndex(data)
	if match != nil {
		return match[1], data[:match[0]], nil
	}
	if atEOF {
		return len(data), data, nil
	}
	return 0, nil, nil
}

func RunScript(db Querier, rd io.Reader, ignoreErr bool) error {
	scn := bufio.NewScanner(rd)
	scn.Split(splitPara)
	for scn.Scan() {
		stmt := scn.Text()
		for strings.HasPrefix(stmt, "--") {
			if idx := strings.IndexByte(stmt, '\n'); idx > 0 {
				stmt = stmt[idx+1:]
			} else {
				break
			}
		}
		_, err := db.Exec(stmt)
		if !ignoreErr && err != nil {
			return err
		}
	}
	return nil
}

func RunScriptFile(db Querier, file string, ignoreErr bool) error {
	// #nosec G304 -- file is deliberately opened from variable
	rd, err := os.Open(file)
	if err != nil {
		return err
	}
	// #nosec G307
	defer rd.Close()
	return RunScript(db, rd, ignoreErr)
}
