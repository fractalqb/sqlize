package sqlize

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"log"
	"strings"
	"sync/atomic"

	"github.com/ngrok/sqlmw"
)

// LogDriver registers a new sql driver that wraps the real driver drv with
// calls to the Logger logTo. With flags one can select which driver methods
// are logged. If logTo is nil the driver will log to the standard logger
// from Go's log package.
func LogDriver(name string, drv driver.Driver, logTo Logger, flags LogFlag) {
	if logTo == nil {
		logTo = NewStdLogger(nil)
	}
	sql.Register(name, sqlmw.Driver(drv, logInterceptor{logTo, flags}))
}

type CallRef struct {
	Name string
	ID   any
}

type Logger interface {
	Call(name string, args ...any) CallRef
	Result(ref CallRef, err error, args ...any)
}

type LogFlag uint

const (
	LogBeginTx LogFlag = (1 << iota)
	LogPrepare
	LogPing
	LogExec
	LogQuery
	LogConnect
	LogInsertId
	LogRowsAffected
	LogRowsNext
	LogRowsClose
	LogStmtExec
	LogStmtQuery
	LogStmtClose
	LogTxCommit
	LogTxRollback

	LogDefault = LogExec | LogQuery | LogStmtExec | LogStmtQuery | LogInsertId

	LogAll = ^LogFlag(0)
)

type logInterceptor struct {
	Log   Logger
	Flags LogFlag
}

func (ld logInterceptor) ConnBeginTx(ctx context.Context, conn driver.ConnBeginTx, txOpts driver.TxOptions) (context.Context, driver.Tx, error) {
	var ref CallRef
	if ld.Flags&LogBeginTx != 0 {
		ref = ld.Log.Call("BeginTx", txOpts)
	}
	t, err := conn.BeginTx(ctx, txOpts)
	if ld.Flags&LogBeginTx != 0 {
		ld.Log.Result(ref, err)
	}
	return ctx, t, err
}

func (ld logInterceptor) ConnPrepareContext(ctx context.Context, conn driver.ConnPrepareContext, query string) (context.Context, driver.Stmt, error) {
	var ref CallRef
	if ld.Flags&LogPrepare != 0 {
		ref = ld.Log.Call("Prepare", query)
	}
	s, err := conn.PrepareContext(ctx, query)
	if ld.Flags&LogPrepare != 0 {
		ld.Log.Result(ref, err)
	}
	return ctx, s, err
}

func (ld logInterceptor) ConnPing(ctx context.Context, conn driver.Pinger) error {
	var ref CallRef
	if ld.Flags&LogPing != 0 {
		ref = ld.Log.Call("Ping")
	}
	err := conn.Ping(ctx)
	if ld.Flags&LogPing != 0 {
		ld.Log.Result(ref, err)
	}
	return err
}

func (ld logInterceptor) ConnExecContext(ctx context.Context, conn driver.ExecerContext, query string, args []driver.NamedValue) (driver.Result, error) {
	var ref CallRef
	if ld.Flags&LogExec != 0 {
		ref = ld.Log.Call("Exec", query, args)
	}
	res, err := conn.ExecContext(ctx, query, args)
	if ld.Flags&LogExec != 0 {
		ld.Log.Result(ref, err)
	}
	return res, err
}

func (ld logInterceptor) ConnQueryContext(ctx context.Context, conn driver.QueryerContext, query string, args []driver.NamedValue) (context.Context, driver.Rows, error) {
	var ref CallRef
	if ld.Flags&LogQuery != 0 {
		ref = ld.Log.Call("Query", query, args)
	}
	r, err := conn.QueryContext(ctx, query, args)
	if ld.Flags&LogQuery != 0 {
		ld.Log.Result(ref, err)
	}
	return ctx, r, err
}

func (ld logInterceptor) ConnectorConnect(ctx context.Context, connect driver.Connector) (driver.Conn, error) {
	var ref CallRef
	if ld.Flags&LogConnect != 0 {
		ref = ld.Log.Call("Connect")
	}
	res, err := connect.Connect(ctx)
	if ld.Flags&LogConnect != 0 {
		ld.Log.Result(ref, err)
	}
	return res, err
}

func (ld logInterceptor) ResultLastInsertId(res driver.Result) (int64, error) {
	var ref CallRef
	if ld.Flags&LogInsertId != 0 {
		ref = ld.Log.Call("InsertId")
	}
	id, err := res.LastInsertId()
	if ld.Flags&LogInsertId != 0 {
		ld.Log.Result(ref, err, id)
	}
	return id, err
}

func (ld logInterceptor) ResultRowsAffected(res driver.Result) (int64, error) {
	var ref CallRef
	if ld.Flags&LogRowsAffected != 0 {
		ref = ld.Log.Call("RowsAffected")
	}
	n, err := res.RowsAffected()
	if ld.Flags&LogRowsAffected != 0 {
		ld.Log.Result(ref, err, n)
	}
	return n, err
}

func (ld logInterceptor) RowsNext(ctx context.Context, rows driver.Rows, dest []driver.Value) error {
	var ref CallRef
	if ld.Flags&LogRowsNext != 0 {
		ref = ld.Log.Call("RowsNext")
	}
	err := rows.Next(dest)
	if ld.Flags&LogRowsNext != 0 {
		ld.Log.Result(ref, err)
	}
	return err
}

func (ld logInterceptor) RowsClose(ctx context.Context, rows driver.Rows) error {
	var ref CallRef
	if ld.Flags&LogRowsClose != 0 {
		ref = ld.Log.Call("RowsClose")
	}
	err := rows.Close()
	if ld.Flags&LogRowsClose != 0 {
		ld.Log.Result(ref, err)
	}
	return err
}

func (ld logInterceptor) StmtExecContext(ctx context.Context, stmt driver.StmtExecContext, _ string, args []driver.NamedValue) (driver.Result, error) {
	var ref CallRef
	if ld.Flags&LogStmtExec != 0 {
		ref = ld.Log.Call("StmtExec", args)
	}
	res, err := stmt.ExecContext(ctx, args)
	if ld.Flags&LogStmtExec != 0 {
		ld.Log.Result(ref, err)
	}
	return res, err
}

func (ld logInterceptor) StmtQueryContext(ctx context.Context, stmt driver.StmtQueryContext, _ string, args []driver.NamedValue) (context.Context, driver.Rows, error) {
	var ref CallRef
	if ld.Flags&LogStmtQuery != 0 {
		ref = ld.Log.Call("StmtQuery", args)
	}
	r, err := stmt.QueryContext(ctx, args)
	if ld.Flags&LogStmtQuery != 0 {
		ld.Log.Result(ref, err)
	}
	return ctx, r, err
}

func (ld logInterceptor) StmtClose(ctx context.Context, stmt driver.Stmt) error {
	var ref CallRef
	if ld.Flags&LogStmtClose != 0 {
		ref = ld.Log.Call("StmtClose")
	}
	err := stmt.Close()
	if ld.Flags&LogStmtClose != 0 {
		ld.Log.Result(ref, err)
	}
	return err
}

func (ld logInterceptor) TxCommit(ctx context.Context, tx driver.Tx) error {
	var ref CallRef
	if ld.Flags&LogTxCommit != 0 {
		ref = ld.Log.Call("TxCommit")
	}
	err := tx.Commit()
	if ld.Flags&LogTxCommit != 0 {
		ld.Log.Result(ref, err)
	}
	return err
}

func (ld logInterceptor) TxRollback(ctx context.Context, tx driver.Tx) error {
	var ref CallRef
	if ld.Flags&LogTxRollback != 0 {
		ref = ld.Log.Call("TxRollback")
	}
	err := tx.Rollback()
	if ld.Flags&LogTxRollback != 0 {
		ld.Log.Result(ref, err)
	}
	return err
}

type stdLogger struct {
	l *log.Logger
	n uint64
}

// NewStdLogger creates a new SQL logger that logs with Go's standard log
// package. If l is nil the log.Default() logger is used.
func NewStdLogger(l *log.Logger) Logger {
	if l == nil {
		return &stdLogger{l: log.Default()}
	}
	return &stdLogger{l: l}
}

func (l *stdLogger) Call(name string, args ...any) CallRef {
	for i, a := range args {
		if s, ok := a.(string); ok {
			args[i] = rmLineBreaks(s)
		}
	}
	r := atomic.AddUint64(&l.n, 1)
	if args == nil {
		l.l.Printf("%d     %s", r, name)
	} else {
		l.l.Printf("%d     %s %+v", r, name, args)
	}
	return CallRef{name, r}
}

func (l *stdLogger) Result(ref CallRef, err error, args ...any) {
	for i, a := range args {
		if s, ok := a.(string); ok {
			args[i] = rmLineBreaks(s)
		}
	}
	if err == nil {
		if args == nil {
			l.l.Printf("%d OK  %s", ref.ID, ref.Name)
		} else {
			l.l.Printf("%d OK  %s %+v", ref.ID, ref.Name, args)
		}
	} else {
		if args == nil {
			l.l.Printf("%d ERR %s %s", ref.ID, ref.Name, err)
		} else {
			l.l.Printf("%d ERR %s %s (%+v)", ref.ID, ref.Name, err, args)
		}
	}
}

func rmLineBreaks(msg string) string {
	msg = strings.ReplaceAll(msg, "\r", "")
	msg = strings.ReplaceAll(msg, "\n", "↩")
	return msg
}
