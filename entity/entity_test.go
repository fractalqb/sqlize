package entity

import (
	"context"
	"database/sql"
	"testing"

	"git.fractalqb.de/fractalqb/sqlize"
)

type testEty struct {
	Base[ID32]
	Name string
}

func (e *testEty) IsZero() bool { return e == nil }

func (*testEty) Zero() Entity[ID32] { return (*testEty)(nil) }

type testEtyRef = Ref[testEty, ID32]

var _ Entity[ID32] = testEty{}

type testEtyRepo map[ID32]*testEty

func (r testEtyRepo) ReadContext(_ context.Context, _ sqlize.Querier, id ID32, _ *testEty) (*testEty, error) {
	e := r[id]
	if e == nil {
		return nil, sql.ErrNoRows
	}
	return e, nil
}

var testEtys = make(testEtyRepo)

func TestEntityBase_IDScanner(t *testing.T) {
	var e Base[ID32]
	err := e.IDScanner().Scan(int64(4711))
	if err != nil {
		t.Fatal(err)
	}
	if e.id != 4711 {
		t.Errorf("Wrong ID: %d, want 4711", e.id)
	}
}

func TestRef(t *testing.T) {
	ttr := testEtyRepo{
		1: &testEty{
			Base: Base[ID32]{id: 4711},
			Name: "thing 1",
		},
	}
	var ref testEtyRef
	if !ref.Nil() {
		t.Error("Zero Ref is not Nil()")
	}
	ref = RefID[testEty, ID32](ttr, 1)
	if is, e := ref.Resolved(); is {
		t.Error("ref not yet resolved")
	} else if e != nil {
		t.Errorf("git entity from unresolved ref: %+v", e)
	}
	e, err := ref.Get(nil)
	if err != nil {
		t.Fatal(err)
	}
	if e == nil {
		t.Fatal("Failed to resolve ref")
	}
}
