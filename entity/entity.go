package entity

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"fmt"

	"git.fractalqb.de/fractalqb/sqlize"
)

type ID interface{ ID16 | ID32 | ID64 }

type Entity[I ID] interface {
	EntityID() I
}

type ePtr[E Entity[I], I ID] interface {
	*E
	Entity[I]
	IDScanner() sql.Scanner
}

type Base[I ID] struct{ id I }

func (eb Base[I]) EntityID() I { return eb.id }

func (eb *Base[ID]) IDScanner() sql.Scanner {
	var idi any = &eb.id
	switch idp := idi.(type) {
	case *ID64:
		return idp
	case *ID32:
		return idp
	case *ID16:
		return idp
	}
	panic("EntityBase.IDScanner(): unreachable")
}

type Reader[E Entity[I], I ID] interface {
	ReadContext(ctx context.Context, q sqlize.Querier, id I, reuse *E) (*E, error)
}

type Ref[E Entity[I], I ID] struct{ r any }

type stub[E Entity[I], I ID] struct {
	rd Reader[E, I]
	id I
}

func RefID[E Entity[I], I ID](r Reader[E, I], id I) Ref[E, I] {
	return Ref[E, I]{r: stub[E, I]{rd: r, id: id}}
}

func RefEntity[E Entity[I], I ID](entity *E) Ref[E, I] {
	if entity == nil {
		return Ref[E, I]{}
	}
	return Ref[E, I]{r: entity}
}

func (ref Ref[E, ID]) Nil() bool { return ref.r == nil }

func (ref *Ref[E, ID]) Clear() { ref.r = nil }

func (ref *Ref[E, ID]) Set(entity *E) {
	if (*entity).EntityID() == sqlize.NoID {
		ref.Clear()
	} else {
		ref.r = entity
	}
}

func (ref *Ref[E, ID]) SetID(r Reader[E, ID], id ID) error {
	if id == sqlize.NoID {
		ref.Clear()
		return nil
	}
	if r == nil {
		if stub, ok := ref.r.(stub[E, ID]); ok {
			r = stub.rd
		} else {
			var e E
			return fmt.Errorf("no reader when setting Ref[%T] to id %d", e, id)
		}
	}
	ref.r = stub[E, ID]{rd: r, id: id}
	return nil
}

func (ref *Ref[E, ID]) Get(db sqlize.Querier) (*E, error) {
	return ref.GetContext(context.Background(), db)
}

func (ref *Ref[E, ID]) GetContext(ctx context.Context, db sqlize.Querier) (entity *E, err error) {
	switch to := ref.r.(type) {
	case *E:
		return to, nil
	case stub[E, ID]:
		entity, err = to.rd.ReadContext(ctx, db, to.id, nil)
		if err == nil {
			ref.r = entity
		}
		return entity, err
	case nil:
		return nil, nil
	}
	panic(fmt.Errorf(invTT, ref.r))
}

func (ref Ref[E, ID]) ID() ID {
	switch to := ref.r.(type) {
	case nil:
		return sqlize.NoID
	case stub[E, ID]:
		return to.id
	case *E:
		return (*to).EntityID()
	}
	panic(fmt.Errorf(invTT, ref.r))
}

func (ref Ref[E, ID]) Resolved() (is bool, entity *E) {
	switch t := ref.r.(type) {
	case *E:
		return true, t
	case stub[E, ID]:
		return false, nil
	case nil:
		return true, nil
	}
	panic(fmt.Errorf(invTT, ref.r))
}

func (ref *Ref[E, ID]) SetReader(r Reader[E, ID]) {
	switch p := ref.r.(type) {
	case stub[E, ID]:
		ref.r = stub[E, ID]{rd: r, id: p.id}
	case *E:
		ref.r = stub[E, ID]{rd: r, id: (*p).EntityID()}
	case nil:
		ref.r = stub[E, ID]{rd: r, id: sqlize.NoID}
	}
}

func (ref Ref[E, ID]) WithReader(r Reader[E, ID]) (res Ref[E, ID]) {
	switch p := ref.r.(type) {
	case stub[E, ID]:
		res.r = stub[E, ID]{rd: r, id: p.id}
	case *E:
		res.r = stub[E, ID]{rd: r, id: (*p).EntityID()}
	case nil:
		res.r = stub[E, ID]{rd: r, id: sqlize.NoID}
	}
	return res
}

func (ref Ref[E, ID]) Value() (driver.Value, error) {
	id := ref.ID()
	if id == sqlize.NoID {
		return nil, nil
	}
	return int64(id), nil
}

func (ref *Ref[E, I]) Scanner(r Reader[E, I]) refScanner[E, I] {
	return refScanner[E, I]{ref, r}
}

type refScanner[E Entity[I], I ID] struct {
	ref *Ref[E, I]
	rd  Reader[E, I]
}

func (s refScanner[E, ID]) Scan(src interface{}) error {
	var (
		dummy ID
		idi   any = dummy
	)
	switch idi.(type) {
	case ID64:
		var tmp ID64
		if err := tmp.Scan(src); err != nil {
			return err
		}
		return s.ref.SetID(s.rd, ID(tmp))
	case ID32:
		var tmp ID32
		if err := tmp.Scan(src); err != nil {
			return err
		}
		return s.ref.SetID(s.rd, ID(tmp))
	case ID16:
		var tmp ID16
		if err := tmp.Scan(src); err != nil {
			return err
		}
		return s.ref.SetID(s.rd, ID(tmp))
	}
	panic(fmt.Sprintf("Inavlid EntityID type %T", dummy))
}

const invTT = "invalid Ref target type %T"
