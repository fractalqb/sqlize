package entity

import "fmt"

func ExampleIDs_ID32() {
	type Dummy struct{ Base[ID32] }
	var d Dummy
	fmt.Println(d.IDScanner().Scan(int64(4711)))
	fmt.Println(d.EntityID())
	// Output:
	// <nil>
	// 4711
}
