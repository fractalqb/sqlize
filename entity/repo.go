package entity

import (
	"context"
	"database/sql"

	"git.fractalqb.de/fractalqb/sqlize"
	"git.fractalqb.de/fractalqb/sqlize/bsq"
)

type Repo[E Entity[I], I ID] interface {
	Reader[E, I]
	Read(q sqlize.Querier, id I, reuse *E) (*E, error)
	Create(q sqlize.Querier, entity *E) (*E, error)
	CreateContext(ctx context.Context, q sqlize.Querier, entity *E) (*E, error)
	Update(q sqlize.Querier, entity *E) error
	UpdateContext(ctx context.Context, q sqlize.Querier, entity *E) error
	Delete(q sqlize.Querier, id I) error
	DeleteContext(ctx context.Context, q sqlize.Querier, id I) error
}

type DBRepo[E Entity[I], I ID, P ePtr[E, I]] struct {
	fieldMap *FieldMap[E]
	hint     any
	qCreate  *bsq.Query
	qSelect  *bsq.Query
	qUpdate  *bsq.Query
	qDelete  *bsq.Query
}

func NewDBRepo[E Entity[I], I ID, P ePtr[E, I]](
	d bsq.Dialect,
	id *bsq.Column,
	m *FieldMap[E],
	hint any,
) (repo *DBRepo[E, I, P], err error) {
	repo = &DBRepo[E, I, P]{fieldMap: m, hint: hint}
	if repo.hint == nil {
		repo.hint = repo
	}
	repo.qCreate, err = bsq.NewCreateQuery(d, id)
	if err != nil {
		return nil, err
	}
	repo.qSelect, err = bsq.NewQuery(d, bsq.Select{Key: bsq.Columns{id}})
	if err != nil {
		return nil, err
	}
	repo.qUpdate, err = bsq.NewQuery(d, bsq.Update{Key: bsq.Columns{id}})
	if err != nil {
		return nil, err
	}
	repo.qDelete, err = bsq.NewQuery(d, bsq.Delete{Key: bsq.Columns{id}})
	if err != nil {
		return nil, err
	}
	return repo, nil
}

func (r *DBRepo[E, I, P]) Prepare(db *sql.DB) error {
	return r.PrepareContext(context.Background(), db)
}

func (r *DBRepo[E, I, P]) PrepareContext(ctx context.Context, db *sql.DB) error {
	if err := r.qCreate.PrepareContext(ctx, db); err != nil {
		return err
	}
	if err := r.qSelect.PrepareContext(ctx, db); err != nil {
		return err
	}
	if err := r.qUpdate.PrepareContext(ctx, db); err != nil {
		return err
	}
	return r.qDelete.PrepareContext(ctx, db)
}

func (r *DBRepo[E, I, P]) Create(q sqlize.Querier, entity P) (*E, error) {
	return r.CreateContext(context.Background(), q, entity)
}

func (r *DBRepo[E, I, P]) CreateContext(ctx context.Context, q sqlize.Querier, entity P) (*E, error) {
	pCols := r.qCreate.ParamCols()
	args := make([]any, len(pCols))
	for i := range args {
		args[i] = r.fieldMap.ToSQL(pCols[i].Index())(entity, r.hint)
	}
	id, err := bsq.QueryCreateContext(ctx, q, r.qCreate, args...)
	if err != nil {
		return entity, err
	}
	if err = entity.IDScanner().Scan(id); err != nil {
		return entity, err
	}
	return entity, nil
}

func (r *DBRepo[E, I, P]) Read(q sqlize.Querier, id I, reuse P) (*E, error) {
	return r.ReadContext(context.Background(), q, id, reuse)
}

func (r *DBRepo[E, I, P]) ReadContext(ctx context.Context, q sqlize.Querier, id I, reuse P) (*E, error) {
	if reuse == nil {
		reuse = new(E)
	}
	oCols := r.qSelect.OutCols()
	outs := make([]any, len(oCols))
	for i := range outs {
		outs[i] = r.fieldMap.FromSQL(oCols[i].Index())(reuse, r.hint)
	}
	err := q.QueryRow(r.qSelect.String(), id).Scan(outs...)
	if err == nil {
		reuse.IDScanner().Scan(id)
	}
	return reuse, err
}

func (r *DBRepo[E, _, _]) Update(q sqlize.Querier, entity *E) error {
	return r.UpdateContext(context.Background(), q, entity)
}

func (r *DBRepo[E, _, _]) UpdateContext(ctx context.Context, q sqlize.Querier, entity *E) error {
	pCols := r.qUpdate.ParamCols()
	args := make([]any, len(pCols))
	for i := range args {
		args[i] = r.fieldMap.ToSQL(pCols[i].Index())(entity, r.hint)
	}
	if _, err := q.ExecContext(ctx, r.qUpdate.String(), args...); err != nil {
		return err
	}
	return nil
}

func (r *DBRepo[E, I, _]) Delete(q sqlize.Querier, id I) error {
	_, err := q.Exec(r.qDelete.String(), id)
	return err
}

func (r *DBRepo[E, I, _]) DeleteContext(ctx context.Context, q sqlize.Querier, id I) error {
	_, err := q.ExecContext(ctx, r.qDelete.String(), id)
	return err
}
