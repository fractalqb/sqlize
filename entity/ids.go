package entity

import (
	"database/sql/driver"
	"fmt"
	"math"
)

type ID16 int16

func (id ID16) Value() (driver.Value, error) {
	if id <= 0 {
		return nil, nil
	}
	return int64(id), nil
}

func (id *ID16) Scan(src any) error {
	switch src := src.(type) {
	case int64:
		if src < 0 || src > math.MaxInt16 {
			return fmt.Errorf("%d out of ID16 range", src)
		}
		*id = ID16(src)
	case ID16:
		*id = src
	case nil:
		*id = 0
	default:
		return fmt.Errorf("cannot scan ID16 from %T", src)
	}
	return nil
}

type ID32 int32

func (id ID32) Value() (driver.Value, error) {
	if id <= 0 {
		return nil, nil
	}
	return int64(id), nil
}

func (id *ID32) Scan(src any) error {
	switch src := src.(type) {
	case int64:
		if src < 0 || src > math.MaxInt32 {
			return fmt.Errorf("%d out of ID32 range", src)
		}
		*id = ID32(src)
	case ID32:
		*id = src
	case nil:
		*id = 0
	default:
		return fmt.Errorf("cannot scan ID32 from %T", src)
	}
	return nil
}

type ID64 int64

func (id ID64) Value() (driver.Value, error) {
	if id <= 0 {
		return nil, nil
	}
	return int64(id), nil
}

func (id *ID64) Scan(src any) error {
	switch src := src.(type) {
	case int64:
		*id = ID64(src)
	case ID64:
		*id = src
	case nil:
		*id = 0
	default:
		return fmt.Errorf("cannot scan ID64 from %T", src)
	}
	return nil
}
