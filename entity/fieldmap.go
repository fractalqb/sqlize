package entity

import (
	"fmt"
	"reflect"
	"slices"
	"strings"

	"git.fractalqb.de/fractalqb/sqlize/bsq"
)

type MapFunc[E any] func(entity *E, hint any) any

type FieldMap[E any] struct {
	table   bsq.TableDef
	toSQL   []MapFunc[E]
	fromSQL []MapFunc[E]
}

func (m *FieldMap[E]) Clone() *FieldMap[E] {
	res := &FieldMap[E]{
		toSQL:   slices.Clone(m.toSQL),
		fromSQL: slices.Clone(m.fromSQL),
	}
	return res
}

func (m *FieldMap[E]) MapToAndFrom(col *bsq.Column, toSQL, fromSQL MapFunc[E]) error {
	if err := m.checkCol(col); err != nil {
		return err
	}
	idx := col.Index()
	m.ensureSize(idx + 1)
	m.toSQL[idx] = toSQL
	m.fromSQL[idx] = fromSQL
	return nil
}

func (m *FieldMap[E]) MapBoth(col *bsq.Column, tofrom MapFunc[E]) error {
	return m.MapToAndFrom(col, tofrom, tofrom)
}

func (m *FieldMap[E]) MapTo(col *bsq.Column, to MapFunc[E]) error {
	if err := m.checkCol(col); err != nil {
		return err
	}
	idx := col.Index()
	m.ensureSize(idx + 1)
	m.toSQL[idx] = to
	return nil
}

func (m *FieldMap[E]) MapFrom(col *bsq.Column, from MapFunc[E]) error {
	if err := m.checkCol(col); err != nil {
		return err
	}
	idx := col.Index()
	m.ensureSize(idx + 1)
	m.fromSQL[idx] = from
	return nil
}

func (m *FieldMap[E]) BindOut(e *E, hint any, reuse []any, cols ...*bsq.Column) []any {
	if cap(reuse) < len(cols) {
		reuse = make([]any, len(cols))
	} else {
		reuse = reuse[:len(cols)]
	}
	for i, col := range cols {
		if col.Table() == m.table {
			reuse[i] = m.FromSQL(col.Index())(e, hint)
		}
	}
	return reuse
}

func (m *FieldMap[E]) BindParams(e *E, hint any, reuse []any, cols ...*bsq.Column) []any {
	if cap(reuse) < len(cols) {
		reuse = make([]any, len(cols))
	} else {
		reuse = reuse[:len(cols)]
	}
	for i, col := range cols {
		if col.Table() == m.table {
			reuse[i] = m.ToSQL(col.Index())(e, hint)
		}
	}
	return reuse
}

func (m *FieldMap[E]) ToSQL(i int) MapFunc[E]   { return m.toSQL[i] }
func (m *FieldMap[E]) FromSQL(i int) MapFunc[E] { return m.fromSQL[i] }

func MapIDField[E Entity[I], I ID, P ePtr[E, I]](m *FieldMap[E], col *bsq.Column) error {
	return m.MapToAndFrom(col,
		func(e *E, _ any) any { return (*e).EntityID() },
		func(e *E, _ any) any { return P(e).IDScanner() },
	)
}

type MapMode uint

const (
	MapAllTo MapMode = (1 << iota)
	MapAllFrom

	MapAll = MapAllTo | MapAllFrom
)

func (m *FieldMap[E]) MapMethods(mode MapMode, cols ...*bsq.Column) error {
	type mapMeths struct{ to, from reflect.Method }
	colMaps := make(map[string]mapMeths)
	ety := reflect.TypeFor[*E]()
	for i := 0; i < ety.NumMethod(); i++ {
		m := ety.Method(i)
		if f, mf := isMapMethod(m); f != "" {
			cm := colMaps[f]
			if mf&MapAllTo != 0 {
				cm.to = m // TODO check ambiguities MapSQL / ToSQL
			}
			if mf&MapAllFrom != 0 {
				cm.from = m // TODO check ambiguities MapSQL / FromSQL
			}
			colMaps[f] = cm
		}
	}
	for _, col := range cols {
		cm, ok := colMaps[col.Name]
		if !ok && mode != 0 {
			var e E
			return fmt.Errorf("no mapping for %s in entity %T", col.QName(), e)
		}
		if cm.to.Name == "" && mode&MapAllTo != 0 {
			var e E
			return fmt.Errorf("entity %T has no mapping to %s", e, col.QName())
		}
		if cm.from.Name == "" && mode&MapAllFrom != 0 {
			var e E
			return fmt.Errorf("entity %T has no mapping from %s", e, col.QName())
		}
		m.MapToAndFrom(col,
			cm.to.Func.Interface().(func(*E, any) any),
			cm.from.Func.Interface().(func(*E, any) any),
		)
	}
	return nil
}

func isMapMethod(m reflect.Method) (field string, mode MapMode) {
	switch {
	case strings.HasPrefix(m.Name, "ToSQL"):
		field = m.Name[5:]
		mode = MapAllTo
	case strings.HasPrefix(m.Name, "FromSQL"):
		field = m.Name[7:]
		mode = MapAllFrom
	case strings.HasPrefix(m.Name, "MapSQL"):
		field = m.Name[6:]
		mode = MapAllTo | MapAllFrom
	default:
		return "", 0
	}
	mt := m.Type
	if mt.IsVariadic() {
		return "", 0
	}
	if mt.NumIn() != 2 || mt.NumOut() != 1 {
		return "", 0
	}
	at := reflect.TypeFor[any]()
	if mt.In(1) != at || mt.Out(0) != at {
		return "", 0
	}
	return
}

func (m *FieldMap[E]) ensureSize(s int) {
	if s <= len(m.toSQL) {
		return
	}
	l := 2 * s
	tmp := make([]MapFunc[E], l)
	copy(tmp, m.toSQL)
	copy(tmp[s:], m.fromSQL)
	m.toSQL = tmp[:s:s]
	m.fromSQL = tmp[s:l:l]
}

func (m *FieldMap[E]) checkCol(col *bsq.Column) error {
	if m.table == nil {
		m.table = col.Table()
		return nil
	}
	if m.table != col.Table() {
		return fmt.Errorf("adding column %s to field map of table %s",
			col.QName(),
			m.table.TableDef().QName(),
		)
	}
	return nil
}
