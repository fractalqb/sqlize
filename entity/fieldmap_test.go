package entity

import (
	"fmt"
	"testing"

	"git.fractalqb.de/fractalqb/sqlize/bsq"
	"git.fractalqb.de/fractalqb/testerr"
)

type tBase struct {
	id int
}

func (b *tBase) ToSQLcol1(any) any   { return b.id }
func (b *tBase) FromSQLcol1(any) any { return &b.id }

type tDrvd struct {
	tBase
	name string
}

func (d *tDrvd) ToSQLcol2(any) any   { return d.name }
func (d *tDrvd) FromSQLcol2(any) any { return &d.name }

var tTblB = struct {
	bsq.Table
	Col1 bsq.Column
}{
	Table: bsq.Table{Name: "ttblb"},
	Col1:  bsq.Column{Name: "col1"},
}

var tTblD = struct {
	bsq.Table
	Col2 bsq.Column
}{
	Table: bsq.Table{Name: "ttbld"},
	Col2:  bsq.Column{Name: "col2"},
}

func ExampleMapMethods() {
	bsq.InitSchema("", nil, &tTblB, &tTblD)
	var fm FieldMap[tDrvd]
	fmt.Println(fm.MapMethods(MapAll, bsq.Cols(&tTblD)...))
	// Output:
	// <nil>
}

func TestFieldMap_oneTable(t *testing.T) {
	bsq.InitSchema("", nil, &tTblB, &tTblD)
	var fmap FieldMap[tDrvd]
	testerr.Shall(fmap.MapFrom(&tTblB.Col1, nil)).BeNil(t)
	testerr.Shall(fmap.MapFrom(&tTblD.Col2, nil)).NotNil(t)
}
