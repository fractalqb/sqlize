package entity

import (
	"context"

	"git.fractalqb.de/fractalqb/sqlize"
)

// MapCache is a simple write-through cache for repos that does not implement
// any eviction strategy. It only allows to explicitly [Clear] the cache.
// MapCache implements the Repo interface.
type MapCache[E Entity[I], I ID, P ePtr[E, I]] struct {
	Repo  Repo[E, I]
	cache map[I]*E
}

func NewMapCache[E Entity[I], I ID, P ePtr[E, I]](repo Repo[E, I]) *MapCache[E, I, P] {
	return &MapCache[E, I, P]{
		Repo:  repo,
		cache: make(map[I]*E),
	}
}

func (mc *MapCache[E, I, P]) Clear() { clear(mc.cache) }

func (mc *MapCache[E, I, P]) Read(q sqlize.Querier, id I, reuse P) (*E, error) {
	return mc.ReadContext(context.Background(), q, id, reuse)
}

func (mc *MapCache[E, I, P]) ReadContext(ctx context.Context, q sqlize.Querier, id I, reuse P) (*E, error) {
	if e := mc.cache[id]; e != nil {
		return e, nil
	}
	reuse, err := mc.Repo.ReadContext(ctx, q, id, reuse)
	if err == nil {
		mc.cache[id] = reuse
	}
	return reuse, err
}

func (mc *MapCache[E, I, P]) Create(q sqlize.Querier, entity P) (*E, error) {
	return mc.CreateContext(context.Background(), q, entity)
}

func (mc *MapCache[E, I, P]) CreateContext(ctx context.Context, q sqlize.Querier, entity P) (*E, error) {
	e, err := mc.Repo.CreateContext(ctx, q, entity)
	if err != nil {
		return e, err
	}
	mc.cache[(*e).EntityID()] = e
	return e, nil
}

func (mc *MapCache[E, I, P]) Update(q sqlize.Querier, entity P) error {
	return mc.UpdateContext(context.Background(), q, entity)
}

func (mc *MapCache[E, I, P]) UpdateContext(ctx context.Context, q sqlize.Querier, entity P) error {
	if err := mc.Repo.UpdateContext(ctx, q, entity); err != nil {
		return err
	}
	mc.cache[entity.EntityID()] = entity // Just to be sure
	return nil
}

func (mc *MapCache[E, I, P]) Delete(q sqlize.Querier, id I) error {
	return mc.DeleteContext(context.Background(), q, id)
}

func (mc *MapCache[E, I, P]) DeleteContext(ctx context.Context, q sqlize.Querier, id I) error {
	if err := mc.Repo.DeleteContext(ctx, q, id); err != nil {
		return err
	}
	delete(mc.cache, id)
	return nil
}
