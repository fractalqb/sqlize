package sqlize

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
)

// NoID is an ID that no DB record must have as an ID.
const NoID = 0

func NotNoRows(err error) error {
	if errors.Is(err, sql.ErrNoRows) {
		return nil
	}
	return err
}

// Querier allows to run or execute queries against a database or to prepare
// statements. It is used to drop the distinction between sql.DB and sql.Tx
// in application code. See also Xaction and XactionContext.
type Querier interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	Query(query string, args ...interface{}) (*sql.Rows, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	QueryRow(query string, args ...interface{}) *sql.Row
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
	Prepare(query string) (*sql.Stmt, error)
	PrepareContext(ctx context.Context, query string) (*sql.Stmt, error)
}

// Begin starts a new transaction tx if q is a DB and returns tx, true. If q is
// altrady a transaction tx then tx, false is returned. If an error occurs it
// will be returned as err.
func Begin(q Querier) (tx *sql.Tx, ok bool, err error) {
	switch q := q.(type) {
	case *sql.Tx:
		return q, false, nil
	case *sql.DB:
		tx, err = q.Begin()
		return tx, true, err
	}
	return nil, false, fmt.Errorf("cannot begin transaction from %T", q)
}

func MustBegin(q Querier) (tx *sql.Tx, ok bool) {
	tx, ok, err := Begin(q)
	if err != nil {
		panic(err)
	}
	return tx, ok
}

func BeginTx(q Querier, ctx context.Context, opts *sql.TxOptions) (*sql.Tx, bool, error) {
	switch q := q.(type) {
	case *sql.Tx:
		return q, false, nil
	case *sql.DB:
		tx, err := q.BeginTx(ctx, opts)
		return tx, true, err
	}
	return nil, false, fmt.Errorf("cannot begin transaction from %T", q)
}

func MustBeginTx(q Querier, ctx context.Context, opts *sql.TxOptions) (*sql.Tx, bool) {
	tx, ok, err := BeginTx(q, ctx, opts)
	if err != nil {
		panic(err)
	}
	return tx, ok
}

type RollbackError struct {
	Cause    error
	Rollback error
}

func (ce RollbackError) Error() string {
	return fmt.Sprintf("rollback for '%s' fails with: %s", ce.Cause, ce.Rollback)
}

func (ce RollbackError) Unwrap() error { return ce.Cause }

func CommitOrRollbackOn(err *error, tx *sql.Tx) {
	if *err != nil {
		if re := tx.Rollback(); re != nil {
			*err = RollbackError{Cause: *err, Rollback: re}
		}
	} else if ce := tx.Commit(); ce != nil {
		*err = ce
	}
}

func Xaction(q Querier, do func(*sql.Tx) error) (err error) {
	tx, ok, err := Begin(q)
	if err != nil {
		return err
	}
	if ok {
		defer CommitOrRollbackOn(&err, tx)
	}
	err = do(tx)
	return err
}

func XactionTx(q Querier, ctx context.Context, opts *sql.TxOptions, do func(*sql.Tx) error) (err error) {
	tx, ok, err := BeginTx(q, ctx, opts)
	if err != nil {
		return err
	}
	if ok {
		defer CommitOrRollbackOn(&err, tx)
	}
	err = do(tx)
	return err
}

type ScannerFunc func(any) error

func (f ScannerFunc) Scan(src any) error { return f(src) }
